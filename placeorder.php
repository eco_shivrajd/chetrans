<?php  ini_set('display_errors', 1); 
include 'db.class.php';
$json_string =$_POST['orderarray'];
$array = json_decode($json_string);
$db = new DB();

$order_by_id = $array->sales_person_id;

$total_products = 0;
$order_date = $array->order_date;

$lat=$array->usr_lat;
$long=$array->usr_lng;   
$total_order_cost = 0;
$total_order_gst_cost = 0;
$sale_count = 0;
$free_count = 0;
$offer_provided = 1;
foreach($array->order_list as $key => $order)//order initial details shop level
{
	$order_no = $order->order_id; 
	$shop_id = $order->shop_id;    
	$total_items = $order->total_items;   
	$shop_order_status = 1;//order received
   
	$order_record_id = $db->add_order($order_no, $order_date, $order_by_id, $lat, $long, $shop_id, $shop_order_status);
	if(isset($order->product_details))
	{
		foreach($order->product_details as $pkey => $product)//Product level details
		{	
			$order_product_details = array();
								
			$order_product_details['order_record_id'] = $order_record_id;
			$order_product_details['brand_id'] = $product->brand_id;
			$order_product_details['cat_id'] = $product->category_id;
			$order_product_details['product_id'] =  $product->product_id;
			
			foreach($product->varient_details as $vkey => $varient)//Product variant level details
			{			
				$order_product_details['producthsn'] 		 = $varient->product_hsn;
				$order_product_details['product_variant_id'] = $varient->product_variant_rowcnt;
				$order_product_details['product_quantity'] = $varient->quantity;
				$sale_count+=$varient->quantity;
				
				$order_product_details['product_variant_weight1'] = $varient->weightquantity;
				$order_product_details['product_variant_unit1'] = $varient->unit;
				$order_product_details['product_variant_weight2'] = $varient->weightquantity2;//new param for pcs like variant
				$order_product_details['product_variant_unit2'] = $varient->unit2;//new param for pcs like variant
				$order_product_details['product_unit_cost'] = ($varient->price);/// $varient->quantity);
				
				$total_final_price = 0;
				$discount_apply = 1;
				$cgst_value = 0;
				$sgst_value = 0;
				$total_gst = 0;
				if($varient->campaign_applied == 'yes' && $varient->campaign_type == 'discount'){//Product Campaign level details					
					$order_product_details['campaign_applied'] = 0;
					$order_product_details['campaign_type'] = 'discount';
					$order_product_details['campaign_sale_type'] = 'discount';
					
					$discounted_amount = $varient->price - $varient->campaign_discount;
					$total_final_price =($discounted_amount * $varient->quantity);	
					$discount_apply = 0;
				}else{
					$total_final_price = $varient->price * $varient->quantity;
				}
				$order_product_details['product_total_cost'] = $total_final_price;
				$order_product_details['product_cgst'] = $varient->cgst;//% value
				$order_product_details['product_sgst'] = $varient->sgst;//% value
				if($varient->cgst != 0){
					$cgst_value = (($total_final_price * $varient->cgst)/100);
				}
				
				if($varient->sgst != 0){
					$sgst_value = (($total_final_price * $varient->sgst)/100);
				}
				$total_gst = $total_final_price;
				if($cgst_value != 0)
					$total_gst = $total_gst + $cgst_value;
				if($sgst_value != 0)
					$total_gst = $total_gst + $sgst_value;
								
								
				$order_product_details['p_cost_cgst_sgst'] = $total_gst;
				$total_order_cost+=$total_final_price;
				$total_order_gst_cost+=$total_gst;
				$order_product_details['order_status'] = 1;//order received
				
				$order_variant_id = $db->add_order_details($order_product_details);
				if($discount_apply == 0){
					$db->add_order_c_p_discount($varient->campaign_id,$order_variant_id,$varient->campaign_discount,$varient->campaign_percent,$varient->price);
					$offer_provided = 0;
				}
			}
		}
		$db->update_order($order_record_id, $sale_count, $total_order_cost, $total_order_gst_cost,$offer_provided);
	}
}
include 'wsJSON.php';
$JSONVar = new wsJSON($con);
$total_count = $sale_count + $free_count;
$jsonOutput = $JSONVar->fnplaceorder($total_order_cost,$total_order_gst_cost,$sale_count,$free_count,$total_count);
echo $jsonOutput;