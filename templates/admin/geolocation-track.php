<!DOCTYPE html>
<html>
  <head>
    <!-- This stylesheet contains specific styles for displaying the map
         on this page. Replace it with your own styles as described in the
         documentation:
         https://developers.google.com/maps/documentation/javascript/tutorial -->
    <link rel="stylesheet" href="https://developers.google.com/maps/documentation/javascript/demos/demos.css">
  </head>
  <body>
    <div id="map"></div>
	<?php include "../includes/grid_footer.php"?>
    <script>
      function initMap() {
        var chicago = {lat: 18.490720472647414, lng: 73.89483690261841};
        var indianapolis = {lat: 18.506348498598324, lng: 73.90489518642426};

        var map = new google.maps.Map(document.getElementById('map'), {
          center: chicago,
          zoom: 7
        });

        var directionsDisplay = new google.maps.DirectionsRenderer({
          map: map
        });

        // Set destination, origin and travel mode.
        var request = {
          destination: indianapolis,
          origin: chicago,
          travelMode: 'DRIVING'
        };

        // Pass the directions request to the directions service.
        var directionsService = new google.maps.DirectionsService();
        directionsService.route(request, function(response, status) {
          if (status == 'OK') {
            // Display the route on the map.
            directionsDisplay.setDirections(response);
          }
        });
      }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDMpdO43txdg_zyovvZm9i1tMMFIQTKTU&callback=initMap"
        async defer></script>
  </body>
</html>