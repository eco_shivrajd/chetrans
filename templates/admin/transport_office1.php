<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/commonManage.php";
include "../includes/transportManage.php";
$tObj 		= 	new transportManager($con,$conmain);
$mandatory_mark = '';
if($_SESSION[SESSION_PREFIX."user_type"]=="Admin")  {
	$mandatory_mark = '<span class="mandatory">*</span>';
}
?>
<!-- END HEADER -->
<?php
$id=$_GET['id'];
if(isset($_POST['submit']))
{	
	$tObj->updateTransportOfficeDetails($id);
	echo '<script>alert("Transport Office has been updated successfully.");location.href="transport_offices.php";</script>';
}
?>

<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageTransport"; $activeMenu = "TransportOffices";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<h3 class="page-title">
			Transport Offices
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="Transport Offices.php">Transport Offices</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Transport Office</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Transport Office
							</div>
							
						</div>
						<div class="portlet-body">
						<? if($_SESSION[SESSION_PREFIX."user_type"]=="Admin")  { ?>
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>   
						<? } ?>
						  <?php
						$id=$_GET['id'];
						$row1 = $tObj->getTransportOfficeDetails($id);
						
						?>                         
			<form class="form-horizontal" role="form" method="post" data-parsley-validate="" action="">       
            <div class="form-group">
              <label class="col-md-3">Transport Office Name:<?=$mandatory_mark;?></label>

              <div class="col-md-4">
                <input type="text" name="trans_off_name"
                placeholder="Enter Transport Office Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter Transport Office name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				 
				class="form-control" value="<?=fnStringToHTML($row1['trans_off_name'])?>">
              </div>
            </div><!-- /.form-group -->		
			<div class="form-group">
				  <label class="col-md-3">Transport Type:<span class="mandatory">*</span></label>
				  <div class="col-md-4">								
						<select name="transport_type" id="transport_type" class="form-control"
						data-parsley-trigger="change"				
						data-parsley-required="#true" 
						data-parsley-required-message="Please select Transport Type">
							<option>-select-</option>
							<?php
							$sql="SELECT `id`, `transport_name` FROM tbl_transport_type WHERE status = '0' ORDER BY transport_name";
							$result = mysqli_query($con,$sql);
							while($row = mysqli_fetch_array($result))
							{
								$cat_id=$row['id'];
								$sel="";
								if($row1['transport_type'] == $cat_id)
									$sel="SELECTED";
									
								echo "<option value='$cat_id' $sel>" . $row['transport_name'] . "</option>";
							} ?>
						</select>
				  </div>
				</div><!-- /.form-group -->	
            <div class="form-group">
              <label class="col-md-3">Address:<?=$mandatory_mark;?></label>

              <div class="col-md-4">
                <textarea name="address" rows="4" 
				placeholder="Enter Address"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter address"
				data-parsley-maxlength="200"
				data-parsley-maxlength-message="Only 200 characters are allowed"
				class="form-control"><?php echo fnStringToHTML($row1['address'])?></textarea>
              </div>
            </div><!-- /.form-group -->
            
         <div class="form-group">
			  <label class="col-md-3">State:<?=$mandatory_mark;?></label>
			  <div class="col-md-4">
				<?php
					$sql_state="SELECT * FROM tbl_state where country_id=101";					
					$result_state = mysqli_query($con,$sql_state);		
				?>
				<select name="state_id" class="form-control" onChange="fnShowCity(this.value)" 
				data-parsley-trigger="change"
				data-parsley-required="#true" 
				data-parsley-required-message="Please select State"
				>
					<?php
						if($row1['state_ids'] == 0)
							echo "<option value=''>-Select-</option>";
						while($row_state = mysqli_fetch_array($result_state))
						{
							$selected = "";
							if($row_state['id'] == $row1['state_id'])
								$selected = "selected";				
						
							echo "<option value='".$row_state['id']."' $selected>" . fnStringToHTML($row_state['name']) . "</option>";
						}
					?>
				</select>
			  </div>
			</div><!-- /.form-group -->

			<div class="form-group" id="city_div">
			  <label class="col-md-3">City:<?=$mandatory_mark;?></label>
			  <div class="col-md-4" id="div_select_city">
				<?php
					$selected = '';
					if($row1['city_id'] != '')
					{
						$sql_city="SELECT * FROM tbl_city where state_id='".$row1['state_id']."' ORDER BY name";
						
						$result_city = mysqli_query($con,$sql_city);						
						while($row_city = mysqli_fetch_array($result_city))
						{
							$cat_id=$row_city['id'];
							$selected = "";		
							if($row_city['id'] == $row1['city_id'])
								$selected = "selected";			
						
							$option.= "<option value='$cat_id' $selected>".$row_city['name']."</option>";
						}
					}
					else
						$option = "<option value=''>-Select-</option>";
					
							
				?>
				<select name="city" 
				onchange="FnGetSuburbDropDown(this)" 
				class="form-control"  
				data-parsley-trigger="change"
				data-parsley-required="#true" 
				data-parsley-required-message="Please select City">
				<?=$option;?></select>
			  </div>
			</div><!-- /.form-group --> 

			<div class="form-group" id="area_div">
			  <label class="col-md-3">Region:<?=$mandatory_mark;?></label>
			<?php			
				$sql_area="SELECT `id`, `cityid`, `stateid`, `suburbnm` FROM tbl_surb WHERE cityid = ".$row1['city_id']." and isdeleted!='1' ";				
				$result_area = mysqli_query($con,$sql_area);
				$selected = '';				
			?>
			  <div class="col-md-4"  id="div_select_area">
			  <select name="area" id="area" class="form-control">	
				<option value="">-Select-</option>
				<?php
				while($row_area = mysqli_fetch_array($result_area))
				{	
					$selected = "";		
					if($row_area['id'] == $row1['suburb_id'])
						$selected = "selected";						
					
					echo "<option value='".$row_area['id']."' $selected>" . fnStringToHTML($row_area['suburbnm']) . "</option>";
				}
				?>
				</select>
			  </div>
			</div><!-- /.form-group -->
			<div class="form-group">
			  <label class="col-md-3">Status:</label>
			  <div class="col-md-4">
			  <div class="input-group">					
					<select name="status" id="status" class="form-control">
						<option value="Active" <?php if($row1['status'] == 'Active') echo "selected";?>>Active</option>
						<option value="Inactive" <?php if($row1['status'] == 'Inactive') echo "selected";?>>Inactive</option>
					</select>
				</div>
			  </div>
			</div><!-- /.form-group -->	
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
               <button name="submit" id="submit" class="btn btn-primary">Submit</button>
                <a href="transport_offices.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group -->
          </form>  
                            
 		  
	  	  <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
          <div class="modal-dialog" style="width:300px;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
				<h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4>
				</p>                     
        	  <center><a href="transport_office_delete.php?id=<?php echo $row1['id']?>" ><button type="button" class="btn btn-success">Yes</button></a>
			  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
			  </center>
            </div>    
        </div>
    </div>
</div>		  
                    
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			 </div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>

<script type="text/javascript">

function setSelectNoValue(div,select_element){
	var select_selement_section = '<select name="'+select_element+'" id="'+select_element+'" data-parsley-trigger="change" class="form-control"><option selected disabled value="">-Select-</option></select>';
	document.getElementById(div).innerHTML	=	select_selement_section;
}
function fnShowCity(id_value) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');	
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city&mandatory=mandatory";
	CallAJAX(url,"div_select_city");	
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();	
	$("#subarea").html('<option value="">-Select-</option>');		
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&function_name=FnGetSubareaDropDown";
	CallAJAX(url,"div_select_area");
}
</script> 