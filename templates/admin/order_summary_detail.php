<?php
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$report_title = $orderObj->getReportTitle();

$row = $orderObj->getAllOrders(); 
//print_r($row);
$colspan = "2";
if($_POST['report_type'] == 'salesperson'){
	$colspan = "4";	
}
?>
<?php if($_POST["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<?php } ?>
<table 
	class="table table-striped table-bordered table-hover table-highlight table-checkable" 
	data-provide="datatable" 
	data-display-rows="10"
	data-info="true"
	data-search="true"
	data-length-change="true"
	data-paginate="true"
	id="sample_2">

<thead>
<tr>
	<td colspan="<?=$colspan;?>" align="canter" class="gradeX even" style="text-align:center; font-weight:600;"><h4><b>Regional Head<!-- <?=$report_title;?> --></b></h4></td>              
  </tr>
  <tr>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Name</th>
	<?php if($_POST['report_type'] == 'salesperson'){ ?>
		<th data-filterable="false" data-sortable="true" data-direction="desc">Shop's order count</th>
		<th data-filterable="false" data-sortable="true" data-direction="desc">Shop's no order count</th>
	<?php } ?>
	<th data-filterable="false" data-sortable="true" data-direction="desc">Total Sales ₹</th>              
  </tr>
</thead>
<tbody>					
	<?php 
			foreach($row as $key => $val) { 
			if($_POST['report_type'] == 'salesperson'){				
				$filter_date = $_POST['selTest'];
				$param = "?id=".$val['sp_id']."&param1=".$filter_date;
				if($filter_date == 3)
				{
					$from_date = strtotime($_POST['frmdate']);
					$to_date = strtotime($_POST['todate']);
					$param.="&param2=".$from_date;
					$param.="&param3=".$to_date;
				}
			}
			?>
				<tr class="odd gradeX">
				<td><?=$val['Name'];?></td>
				<?php //if(fnAmountFormat($val['Total_Sales']) == '0.00'){$val['shop_order']= 0;}
				if($_POST['report_type'] == 'salesperson' && $_POST["actionType"]!="excel"){ ?>
					<td><?php if($val['shop_order'] != 0){echo "<a href='shop_orders_summary.php".$param."'>".$val['shop_order']."</a>";}else{echo $val['shop_order'];} ?></td>
					<td><?php if($val['shop_no_order'] != 0){echo "<a href='no_order_history.php".$param."'>".$val['shop_no_order']."</a>";}else{echo $val['shop_no_order'];} ?></td>
				<?php }else if($_POST['report_type'] == 'salesperson' && $_POST["actionType"]=="excel"){ ?>
					<td><?php if($val['shop_order'] != 0){echo $val['shop_order'];}else{echo $val['shop_order'];} ?></td>
					<td><?php if($val['shop_no_order'] != 0){echo $val['shop_no_order'];}else{echo $val['shop_no_order'];} ?></td>
				<?php }?>
				<td align='right'><?=fnAmountFormat($val['Total_Sales']);?></td>									
				</tr>			
	<?php	} 
		if($_POST["actionType"]=="excel" &&  $row == 0) {
			echo "<tr><td>No matching records found</td></tr>";
		}
	?>	
			
</tbody>	
</table>
<script>
jQuery(document).ready(function() {    
   
   ComponentsPickers.init();
});

jQuery(document).ready(function() { 
	TableManaged.init();
});
$(document).ready(function() {
      var table = $('#sample_2').dataTable();
      // Perform a filter
      table.fnFilter('');
      // Remove all filtering
      //table.fnFilterClear();
	   
  });
</script>
<!-- END JAVASCRIPTS -->
<?
if($_POST["actionType"]=="excel") {
	if($row != 0){
		header("Content-Type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=Report_summary.xls");
		exit;
	}
} ?>
 