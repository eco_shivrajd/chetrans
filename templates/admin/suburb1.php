<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/commonManage.php";
if($_SESSION[SESSION_PREFIX.'user_type']!="Admin") 
{
	header("location:../logout.php");
}
$id=$_GET['id'];

if(isset($_POST['submit']))
{
	$name=fnEncodeString($_POST['suburbnm']);
	$state=$_POST['state'];
	$city=$_POST['city'];
	
	$sql_product_check=mysqli_query($con,"select id from `tbl_surb` where suburbnm='$name' and cityid='$city' and isdeleted != 1 and id!='$id'");
	
	if($rowcount = mysqli_num_rows($sql_product_check)>0){	
		echo '<script>alert("Region already exist.");location.href="suburb1.php?id='.$id.'";</script>';
	}else{
		$sql = "UPDATE tbl_surb SET suburbnm='$name',stateid='$state',cityid='$city' where id='$id'";
		$update_sql=mysqli_query($con,$sql);
		$commonObj 	= 	new commonManage($con,$conmain);
		$commonObj->log_update_record('tbl_surb',$id,$sql);
		echo '<script>alert("Region updated successfully.");location.href="suburb.php";</script>';
	}

	
	
	
}

$sql1="SELECT stateid,cityid,suburbnm  FROM tbl_surb where id = '$id' ";
$result1 = mysqli_query($con,$sql1);
$row1 = mysqli_fetch_array($result1);
$stateid = $row1["stateid"];
$cityid = $row1['cityid'];
$suburbnm = fnStringToHTML($row1['suburbnm']);

?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php 
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Area";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">Region</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					
					<li>
						<i class="fa fa-home"></i>
						<a href="suburb.php">Region</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Region</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Region
							</div>							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						  
						<form class="form-horizontal" data-parsley-validate="" role="form" method="post">         
						<div class="form-group">
						  <label class="col-md-3">State:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
						  <select name="state"              
						  data-parsley-trigger="change"				
						  data-parsley-required="#true" 
						  data-parsley-required-message="Please select state"
						  class="form-control" onChange="showUser(this.value)">
						  <option selected disabled>-select-</option>
							<?php
							$sql="SELECT * FROM tbl_state where country_id=101";
							$result = mysqli_query($con,$sql);
							while($row = mysqli_fetch_array($result))
							{
								$cat_id=$row['id'];
								if($stateid == $cat_id)
									$sel="SELECTED";
								else
									$sel="";
								echo "<option value='$cat_id' $sel>" . $row['name'] . "</option>";
							} ?>
							</select>
						  </div>
						</div><!-- /.form-group -->
						
						<div id="Subcategory"></div> 
						
						<div class="form-group">
						  <label class="col-md-3">Region Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text" 
							placeholder="Enter Region Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter Region name"
							data-parsley-maxlength="50"
							data-parsley-maxlength-message="Only 50 characters are allowed"
							name="suburbnm"class="form-control" value="<?=$suburbnm;?>">
						  </div>
						</div><!-- /.form-group -->
						<div class="form-group">
						  <div class="col-md-4 col-md-offset-3">
						   <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
							<a href="suburb.php" class="btn btn-primary">Cancel</a>
						  </div>
						</div><!-- /.form-group -->
					  </form>                                       
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

<style>
.form-horizontal{
font-weight:normal;
}
</style>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<script>  
function showUser(str,action)
{
if (str=="")
{
document.getElementById("Subcategory").innerHTML="";
return;
}
if (window.XMLHttpRequest)
{
xmlhttp=new XMLHttpRequest();
}
else
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
	if (xmlhttp.readyState==4 && xmlhttp.status==200)
	{
		document.getElementById("Subcategory").innerHTML=xmlhttp.responseText;
		if(action=="onload") {
			$("#city").val("<?=$cityid;?>");
		}
	}
}
xmlhttp.open("GET","fetch.php?cat_id="+str,true);
xmlhttp.send();
}
showUser('<?=$stateid;?>','onload');


</script>            