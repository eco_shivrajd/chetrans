<?php
include ("../../includes/config.php");
extract($_POST);
$sqlw = "SELECT o.shop_id,o.total_order_gst_cost,
               od.product_variant_id,od.product_quantity,od.product_variant_weight1,
               od.product_variant_unit1,s.name,p.productname,o.order_date 
               FROM tbl_orders o
               LEFT JOIN tbl_order_details od ON o.id = od.order_id
               LEFT JOIN tbl_product p ON p.id = od.product_id
               LEFT JOIN tbl_shops s ON s.id = o.shop_id WHERE date_format(o.order_date, '%d-%m-%Y') = '".$frmdate."' ";

//while ($rowrw = mysqli_fetch_array ($result1)){
//    echo "<pre>";
 //    print_r($rowrw);
 // }
$condition = "";
		if($dropdownSalesPerson!="")
		{
			$condition .= " AND o.ordered_by = " . $dropdownSalesPerson;
		} 		
		if($dropdownshops !="")
		{
			$condition .= " AND o.shop_id = " . $dropdownshops;
		}		
		if($dropdownbrands  !="")
		{
			$condition .= " AND od.brand_id = " . $dropdownbrands;
		} 		
		if($dropdownProducts  !="")
		{
			$condition .= " AND od.product_id = " . $dropdownProducts;
		}		
		if($dropdownSuburbs !="")
		{
			$condition .= " AND s.suburbid = " . $dropdownSuburbs;
		}		
		if($dropdownCity !="")
		{
			$condition .= " AND s.city = " . $dropdownCity;
		}
		if($dropdownState !="")
		{
			$condition .= " AND s.state = " . $dropdownState;
		}

$sqlw .= $condition;
$sqlw .= " order by s.name";
//echo $sqlw;
$result1 = mysqli_query($con,$sqlw); 
$totalRecords=mysqli_num_rows($result1);
//var_dump($result1);
//$resarr=mysqli_fetch_array($result1);
//$allshopids=array();
//while($rowcount = mysqli_fetch_array($result1)) {
//	$allshopids[] = $rowcount["shopidtemp"];
//}
//$allshopids=array_count_values($allshopids);
?>
<?php if($_GET["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<?php } ?>
<div class="portlet box blue-steel">
	<div class="portlet-title">
		<?php if($_GET["actionType"]!="excel") { ?>
		<div class="caption"><i class="icon-puzzle"></i>Daily Sales Report</div>
		<?php  
		//echo "<pre>";
		//print_r($result1);die();

		if($totalRecords > 0) { ?>
			<button type="button" name="btnExcel" id="btnExcel" onclick="ExportToExcel();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
			&nbsp;
			<button type="button" name="btnPrint" id="btnPrint" onclick="takeprint()" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
		
		<?php } } ?>
	</div>



		<div class="portlet-body">
		<div class="table-responsive" id="dvtblResonsive">
			<table class="table table-striped table-hover table-bordered responsive">
				<thead>
					<tr>
						<th>Date & Time</th>
						<th>Shop name </th>
						<th>Product</th>
						<th>Weight</th>
						<th>Quantity</th>
						<th>Total Price</th>
					</tr>
				</thead>
				<tbody>
				<?php
				 $newarr=array();
				 $newarr1=array();
				while($row = mysqli_fetch_array($result1)) 
				{
					$shopnm = $row["name"];
					//echo $shopnm;
					//exit(); 					
					$Quantity = $row['product_quantity'];					
					$TotalCost = $row['total_order_gst_cost'];
					$total_cost = $Quantity * $TotalCost;			
					$total_cost = number_format($total_cost,2, '.', '');
						
					
					$OtherDetails = "Quantity: " . $Quantity. ", Total Price: " . $total_cost;
														
				
				$newarr['order_date']=date('d-m-Y H:i:s',strtotime($row["order_date"]));
				$newarr['ids']=$row["shop_id"];				
				$newarr['shopnm']=$shopnm;
				//$newarr['ContactPerson']=$ContactPerson;
				$newarr['product_quantity']=$row["product_quantity"];
				$newarr['productname']=$row["productname"];				
				$newarr['weightquantity']=$row["product_quantity"].'-'.$row["product_variant_unit1"];
			//	$newarr['Quantity']=$Quantity.''.$display_icon;
				$newarr['total_cost']=$total_cost;
				$newarr1[]=$newarr;
				 } 
				// echo "<pre>";print_r($newarr1);
				$out = array();
				foreach ($newarr1 as $key => $value){
					$countval=0;
					foreach ($value as $key2 => $value2){
						if($key2=='ids'){
							 $index = $key2.'-'.$value2;
							if (array_key_exists($index, $out)){
								$out[$index]++;
							} else {
								$out[$index] = 1;
							}
						}
						
					}
				}
				$out1 = array_values($out);$j=0;$temp=$newarr1[0]["shopnm"];$sum=0;
				$gtotalq=0;$gtotalp=0;
				for($i=0;$i<count($newarr1);$i++){
				?>
					<tr>
					<?php 
					if($temp==$newarr1[$i]["shopnm"]){						
						$totalq=0;$totalp=0;
							?>	
					<td rowspan='<?php echo $out1[$j];?>'><?php echo date('d-m-Y H:i:s',strtotime($newarr1[$i]["order_date"]));?></td>
						<td rowspan='<?php echo $out1[$j];?>'><?php echo $newarr1[$i]["shopnm"];?></td>
						<?php $sum=$sum+$out1[$j]; $temp=$newarr1[$sum]["shopnm"]; $j++;  } ?>
						<td><?php echo $newarr1[$i]["productname"];?></td>
						<td><?php echo $newarr1[$i]["weightquantity"];?></td>						
						<td><?php echo $newarr1[$i]["product_quantity"];?></td>
						<td><?php echo $newarr1[$i]["total_cost"];?></td>
						<!--". $row["orderid"]. "-->
					</tr>
					<?php 
					$totalq=$totalq+$newarr1[$i]["product_quantity"];
					$totalp=$totalp+$newarr1[$i]["total_cost"];
					if($i==($sum-1)){ 
					$gtotalq=$gtotalq+$totalq;
					$gtotalp=$gtotalp+$totalp;
					?>	
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td><b>Total</b></td>
						<td><b><?php echo  $totalq;?></b></td>
						<td><b><?php echo  $totalp;?></b></td>
					</tr>
						<?php  } ?>	
					<?php
				}
				//echo"<pre>";print_r($out1);
				?>
				<tr>
						<td></td>
						<td></td>
						<td></td>
						<td><b>Grand Total</b></td>
						<td><b><?php echo  $gtotalq;?></b></td>
						<td><b><?php echo  $gtotalp;?></b></td>
					</tr>
				 </tbody>
			</table>
		</div>
	</div>



	

</div>
<?php
if($_GET["actionType"]=="excel") 
{
	header("Content-Type: application/vnd.ms-excel");
	header("Content-disposition: attachment; filename=Report_".$frmdate.".xls");
} 
?>
 