<?php
$html=
"<div id='contentpdf'>
   <table class='table table-bordered-popup'>
				<tbody>
				<tr>
				<td colspan='4' width='70%' class='darkgreen' valign='top'><img src='../../assets/global/img/logo-fh-invoice.jpg' style='width:60px;'> &nbsp; SRI JAYA SHREE FOOD PRODUCTS</td>
				<td colspan='".$colspan3."' class='font-big text-center' valign='top'>Tax Invoice</td>
				</tr>
				
				<tr>
					<td colspan='4' class='fentgreen1'>".$admin_details_basic['address']."<br/>
					Tel: <b>".$admin_details['phone_no']."</b> 
					".$admin_details['website']." Tollfree: <b>".$admin_details['tollfree_no']."</b>  
					State: <b>".$admin_details_basic['state_name']."</b> State Code: <b>".$admin_details_basic['state']."</b> GSTIN :<b>".$admin_details_basic['gst_number_sss']."</b></td>
					<td colspan='".$colspan3."' rowspan='2'>
					<div class='col-md-8 np'>Invoice No.: &nbsp;<span class='blue'>".$order_detail['invoice_no']."</span></div><br/>
					<div class='col-md-8 np'>Dated: &nbsp;<span class='blue'>".date('d/m/Y', strtotime($order_detail['order_details'][1]['delivery_assing_date']))."</span></div><br/>
					<div class='col-md-8 np'>D. C. No.: &nbsp;<span class='blue'>".$order_detail['order_details'][1]['challan_no']."</span></div><br/>
					<div class='col-md-8 np'>Vehicle No.: &nbsp;<span class='blue'>".$order_detail['order_details'][1]['vehicle_no']."</span></div><br/>
					<div class='col-md-8 np'>Transportation Mode: &nbsp;<span class='blue'>".$order_detail['order_details'][1]['transport_mode']."</span></div> <br/>
					<div class='col-md-8 np'>Date & Time of Supply: &nbsp;<span class='blue'>".$order_detail['order_details'][1]['transport_date']."</div></span><br/>
					<div class='col-md-8 np'>Place of Supply: &nbsp;<span class='blue'>".$order_detail['order_details'][1]['place_of_supply']."</span></div>
					</td>
				</tr>
				
				<tr>
					<td colspan='4' valign='top'>Buyer 
					<span class='buyer_section'><b>".$shop_details['name']."</b><br/></span>
					<span class='buyer_section pad-40'>".$shop_details['address'].",<br/></span>
					<span class='buyer_section pad-40'>".$shop_details['city_name']."<br/></span>
					<span class='buyer_section pad-40'>".$shop_details['state_name']."<br/></span>
					<span class='buyer_section pad-40'>GSTIN NO. ".$shop_details['gst_number']."</span>
					</td>                        
				</tr>
				<tr class='fentgreen'>
				<th width='5%' class='text-center'>SI No.</th>
				<th class='text-center'>Name of Goods</th>
				<th class='text-center'>HSN Code</th>
				<th class='text-center'>Qty</th>
				<th class='text-center'>Rate</th>
				<th class='text-center'>UOM</th>";
				 if($order_detail['offer_provided'] != null && $order_detail['offer_provided'] == 0 ){ 
				$html.="<th class='text-center'>Discount</th>";
				 } 
				$html.="<th class='text-center'>Value</th>
				</tr>
				<tr style='height:214px;'>";
				
				$i = 1;
				$final_qty = 0;
				$final_cost = 0;
				
				
				$total_amount = 0;
				$cgst_amount = 0;
				$sgst_amount = 0;
				$cgst_percent = 0;
				$sgst_percent = 0;
				foreach($order_detail['order_details'] as $val){
					$sr_no.=$i.'<br><br>';
					$product_name.=$val['product_variant_weight1'].' '.$val['product_variant_unit1'].' '.$val['product_name'].'<br><br>';
					$hsn.=$val['producthsn'].'<br><br>';
					$qty.=$val['product_quantity'].'<br><br>';
					$final_qty = $final_qty + $val['product_quantity'];
					$unit_cost.=$val['product_unit_cost'].'<br><br>';
					$nos.='nos<br><br>';
					if($val['campaign_applied'] == 0){ 
						$discounted_amt.= ($val['discount_amount'] * $val['product_quantity']).'<br><br>';
					}
					
					$total_cost.=$val['product_total_cost'].'<br><br>';
					$final_cost = $final_cost + $val['product_total_cost'];
									
					
					$cgst_value = (($val['product_total_cost'] * $val['product_cgst'])/100);
					$sgst_value = (($val['product_total_cost'] * $val['product_sgst'])/100);
					$total_amount = $total_amount + $val['product_total_cost'];
					$cgst_amount = $cgst_amount + $cgst_value;
					$sgst_amount = $sgst_amount + $sgst_value;
					$cgst_percent = $cgst_percent + $val['product_cgst'];
					$sgst_percent = $sgst_percent + $val['product_sgst'];
					$i++; 
					} 
					$gst_total_amount = $total_amount + $cgst_amount + $sgst_amount;
					$gst_total_amount_round = ceil($gst_total_amount);
					$round_val = $gst_total_amount_round - $gst_total_amount;
					
					$closing_balance = $gst_total_amount_round + $opening_balance ;
				
				$html.="<td class='text-center' valign='top'><span class='blue'>".$sr_no."</span></td>
				<td class='bg' valign='top'><span class='blue'>".$product_name."</span></td>
				<td class='text-left' valign='top'><span class='blue'>".$hsn."</span></td>
				<td class='text-right' valign='top'><span class='blue'>".$qty."</span></td>
				<td class='text-right' valign='top'><span class='blue'>".$unit_cost."</span></td>
				<td class='text-center' valign='top'><span class='blue'>".$nos."</span></td>";
				if ($order_detail['offer_provided'] != null && $order_detail['offer_provided'] == 0 ){ 
					$html.="<td class='text-right' valign='top'><span class='blue'>".$discounted_amt."</span></td>";
				 } 
				$html.="<td class='text-right' align='right' valign='top'><span class='blue'>".$total_cost."</span></td>
				</tr>
				<tr>
				<td></td>
				<td class='text-right'><b>Total</b></td>
				<td class='fentgreen'></td>
				<td class='fentgreen text-right'>".$final_qty."</td>
				<td class='fentgreen'></td>
				<td class='fentgreen'></td>";
				if ($order_detail['offer_provided'] != null && $order_detail['offer_provided'] == 0 ){ 
					$html.="<td class='fentgreen'></td>";
				 } 
				$html.="<td class='fentgreen' align='right'>".$final_cost."</td>
				</tr>
				
				<tr>
				<td colspan='4' class='text-center blue'><b>".number_to_word($gst_total_amount_round)." only</b>
				
				<table class='table table-bordered-popup'>
				<tbody>
				<tr>
				<td class='text-center'><span class='blue'>HSN/SAC</span></td>
				<td class='text-center'><span class='blue'>Taxable Value</span></td>
				<td colspan='2' class='text-center'><span class='blue'>Central Tax</span> </td>
				<td colspan='2' class='text-center'><span class='blue'>State Tax</span></td>
				</tr>
				
				<tr>
				<td></td>
				<td></td>
				<td><span class='blue'>Rate</span> </td>
				<td><span class='blue'>Amount</span></td>
				<td><span class='blue'>Rate</span> </td>
				<td><span class='blue'>Amount</span></td>
				</tr>";
				 
				$total_amount = 0;
				$cgst_amount = 0;
				$sgst_amount = 0;
				$cgst_percent = 0;
				$sgst_percent = 0;
				foreach($order_detail['order_details'] as $val){ 
					$cgst_value = (($val['product_total_cost'] * $val['product_cgst'])/100);
					$sgst_value = (($val['product_total_cost'] * $val['product_sgst'])/100);
					$total_amount = $total_amount + $val['product_total_cost'];
					$cgst_amount = $cgst_amount + $cgst_value;
					$sgst_amount = $sgst_amount + $sgst_value;
					$cgst_percent = $cgst_percent + $val['product_cgst'];
					$sgst_percent = $sgst_percent + $val['product_sgst'];
				
				$html.="<tr>
				<td><span class='blue'>".$val['producthsn']."</span></td>
				<td class='text-right'><span class='blue'>".$val['product_total_cost']."</span></td>
				<td><span class='blue'>".$val['product_cgst']." %</span></td>
				<td class='text-right'><span class='blue'>".$cgst_value."</span></td>
				<td><span class='blue'>".$val['product_sgst']." %</span></td>
				<td class='text-right'><span class='blue'>".$sgst_value."</span></td>
				</tr>";
				 } 
				$html.="<tr>
				<td class='text-right'><span class='blue'>Total</span></td>
				<td class='text-right'><span class='blue'>".$total_amount."</td>
				<td></td>
				<td class='text-right'><span class='blue'>".$cgst_amount."</span></td>
				<td> </td>
				<td class='text-right'><span class='blue'>".$sgst_amount."</span></td>
				</tr>
				
				</tbody>
				</table>
				
				</td>
				<td colspan='".$colspan2."' rowspan='2' valign='top'>
				<span style='display:inline-block; height:40px;' class='blue'>CGST @ ".$cgst_percent." %</span><br/>
				<span style='display:inline-block; height:40px;' class='blue'>SGST @ ".$sgst_percent." %</span><br/>
				<span style='display:inline-block; height:40px;' class='blue'>Rounding Off</span> <br/>
				<span style='display:inline-block; height:40px;' class='blue'><b>Grand Total</b> </span><br/>
				<span style='display:inline-block; height:40px;' class='blue'>Opening Balance</span> <br/>
				<span style='display:inline-block; height:40px;' class='blue'>Closing Balance </span>
				</td>
				<td rowspan='2' class='text-right' valign='top'>
				<span style='display:inline-block; height:40px;' class='blue'> ".$cgst_amount."</span><br/>
				<span style='display:inline-block; height:40px;' class='blue'>".$sgst_amount."</span><br/>
				
				
				<span style='display:inline-block; height:40px;' class='blue'>".$round_val."</span><br/>
				<span style='display:inline-block; height:40px;' class='blue'><b>".$gst_total_amount_round."</b></span><br/>
				<span style='display:inline-block; height:40px;' class='blue'>".$opening_balance."</span><br/>
				<span style='display:inline-block; height:40px;' class='blue'>".$closing_balance."</span>
				</td>
				</tr>
				
				<tr>
				<td colspan='2' width='30%' valign='top'>
				<u>Declaration:</u><br/>
				".$admin_details['declaration']."
				</td>
				<td><div class='text-center' width='25%' valign='top'><b><u>BANK DETAILS</u></b></div>
				<div class='col-md-5 np'>BANK NAME:</div> ".$admin_details['accbrnm'].",<br/>
				<div class='col-md-5 np'>BRANCH:</div> SALEM MAIN, FIVE ROADS<br/>
				<div class='col-md-5 np'>CC A/C NO.:</div> ".$admin_details['accno']."<br/>
				<div class='col-md-5 np'>IFSC CODE:</div> ".$admin_details['accifsc']."</td>
				<td  class='fentgreen font-big1' width='20%' valign='top'>For <b>SRI JAYA SHREE FOOD PRODUCTS</b><br/><br/><br/>
				Authorised Signature
				</td>
				</tr>
				</tbody>
				
				</table>
</div>";
?>