<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
if($_SESSION[SESSION_PREFIX.'user_type']!="Admin") 
{
	header("location:../logout.php");
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">

	<!-- BEGIN SIDEBAR -->
	<?php 
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Subarea";
	include "../includes/sidebar.php"
	?>	
	<!-- END SIDEBAR -->
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Subarea
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Subarea</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
            <div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Subarea Listing
							</div>
                            <a href="subarea-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Subarea
                              </a>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								 
								<th>
									Subarea Name
								</th>
								<th>
									Region Name
								</th>
								<th>
									City
								</th>
								<th>
									State
								</th>
								  <?php if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") 
								{ ?>
								<th>
                                  Action
                                </th>
								<?php } ?>
							</tr>
							</thead>
							<tbody>
							<?php
							$sql="SELECT 
								subarea.subarea_name, 
								st.name as statename,
								ct.name as cityname,
								surb.suburbnm,
								subarea.subarea_id
							FROM 
								tbl_subarea subarea 
								left join tbl_state st on st.id = subarea.state_id
								left join tbl_city ct on ct.id = subarea.city_id
								left join tbl_surb surb on surb.id = subarea.suburb_id where subarea.isdeleted!='1' ";
							$result1 = mysqli_query($con,$sql);
							while($row = mysqli_fetch_array($result1))
							{						
								echo '<tr class="odd gradeX">
								 
								<td>
									<a href="subarea-edit.php?id='.$row['subarea_id'].'">'.fnStringToHTML($row['subarea_name']).'</a>
								</td>
								<td>'.fnStringToHTML($row['suburbnm']).'</td>
								<td>'.fnStringToHTML($row['cityname']).'</td>
								<td>'.fnStringToHTML($row['statename']).'</td>';
								if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") {
									echo '<td>
										<a href="manageuser.php?utype=Subarea&id='.$row['subarea_id'].'">Delete</a>
									</td>';
								}
								echo '</tr>';
							
							} ?>
							</tbody>
							</table>
							<br>
						</div>
					</div>
            
				
                    
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<script>
$("#select_all").click(function(){
    $('input:checkbox').prop('checked', this.checked);
});
</script>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>