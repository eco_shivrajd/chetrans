<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php"?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include "../includes/sidebar.php"?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Campaign
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Manage</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Campaign</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
            <div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Campaign Listing
							</div>						
                            <a href="campaign-add-html.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Campaign
                              </a>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								
								<th>
									 Campaign Name
								</th>
								<th>
									 Campaign Type
								</th>
                                <th>
									 Start Date
								</th>
								<th>
									 End Date
								</th>
							</tr>
							</thead>
							<tbody>
								<?php
								$sql="SELECT c.id AS campaign_id, c.campaign_name,c.level, c.campaign_start_date,c.campaign_end_date FROM tbl_campaign_old AS c";
								$result = mysqli_query($con,$sql);
								$i=0;
								while($row = mysqli_fetch_array($result))
								{
										if($i%2==0){
										echo '<tr class="odd gradeX">									 
											   <td>
											   <a href="campaign-edit-html.php?id='.$row['campaign_id'].'">'.$row['campaign_name'].'</a>
											   </td>
											   <td>
											   Free Product
											   </td>
												 <td>
											   '.$row['campaign_start_date'].'
											   </td>
												<td>
											   '.$row['campaign_end_date'].'
											   </td>';
										}else{
											echo '<tr class="odd gradeX">									 
											   <td>
											   <a href="campaign-edit-html.php?id='.$row['campaign_id'].'">'.$row['campaign_name'].'</a>
											   </td>
											   <td>
											   Price Discount
											   </td>
												 <td>
											   '.$row['campaign_start_date'].'
											   </td>
												<td>
											   '.$row['campaign_end_date'].'
											   </td>';
										}
											$i++;
								}							
								?>							
							</tbody>
							</table>
						</div>
					</div>
            
				
                    
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>