<? 
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);

//print"<pre>";print_R($_POST);
/*
[select_all] => Array
(
	[0] => 
	[1] => ordersub_2_2
	[2] => ordersub_2_3
	[3] => ordersub_2_4
	[4] => ordersub_1_1
)
*/
$checked_count = count($_POST['select_all']);
?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Set Order Payment</h4>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-12">   
	<div class="portlet box blue-steel">
		<div class="portlet-title ">
			<div class="caption printHeading">
				Orders
			</div>                          
		</div>
		<div class="portlet-body">	
			<form data-parsley-validate="" role="form"  name="order_update" id="order_update"  enctype="multipart/form-data" method="post">	<!--action="order_update.php"-->	
			<table class="table table-striped table-bordered table-hover" width="100%">
			
			<? 
			$start = 0;
			if($_POST['select_all'][0] != '' &&  $_POST['select_all'][0] != 'on'){
					$data_count = $checked_count;
			}else{
				$data_count = $checked_count - 1;
				$start = 1;
			}
			
			for($j=$start; $j<$checked_count; $j++){
				$checked_string = $_POST['select_all'][$j];		
				$part = explode('_',$checked_string);
				$order_id = $part[1];
				$order_details_id = $part[2];
				$order_details = $orderObj->getOrderDetailsById($order_details_id);
				$product_variant = $orderObj->getSProductVariant($order_details['product_variant_id']);
				if($start == 0)
					$i = $j+1;
				else
					$i = $j;
				//print_r($order_details);
			?>
			<tr>
				<td><?=$i;?>) Order Deatil</td>
				<td>
					<b>Date:</b> <?=$order_details['order_date'];?>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<b>ID:</b> <?=$order_details['order_no'];?>
				<br><b>Invoice:</b> <?=$order_details['invoice_no'];?>
				<br><b>Shop:</b> <?=$order_details['shop_name'];?>
				<br><b>Product:</b> <?=$order_details['product_name'].' '.$product_variant;?>
				<br><b>Quantity:</b> <?=$order_details['product_quantity'];?>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<b>Price:</b> <?=$order_details['p_cost_cgst_sgst'];?>
				<?php if($order_details['order_status'] == 8){?>
					<br><b>Paid Amount:</b> <?=$order_details['amount_paid']." on ".fnSiteDateFormat($order_details['payment_date']); ?>
				<?php } ?>
				</td>				
			</tr>
			<tr>
				<td>Payment Amount</td>
				<td>
					<input type="hidden" name="shop_id_<?=$i;?>" id="shop_id_<?=$i;?>" value="<?=$order_details['shop_id'];?>">
					<input type="hidden" name="order_id_<?=$i;?>" id="order_id_<?=$i;?>" value="<?=$order_id;?>">
					<input type="hidden" name="order_details_id_<?=$i;?>" id="order_details_id_<?=$i;?>" value="<?=$order_details_id;?>">
					<?php
					 $amount_to_pay = 0;
					 if($order_details['order_status'] == 8){
						 $amount_to_pay = $order_details['p_cost_cgst_sgst']-$order_details['amount_paid'];
					 }else{
						$amount_to_pay = $order_details['p_cost_cgst_sgst'];
					 }
					?>
					<input type="hidden" name="order_price_<?=$i;?>" id="order_price_<?=$i;?>" value="<?=$amount_to_pay;?>">
					<div style="width: 130px; float: left;">					
						<input type="checkbox" name="payment_partial_<?=$i;?>" id="payment_partial_<?=$i;?>" title="Check for Partial Payment received">
						&nbsp;&nbsp;Partial Payment
					</div>
					<div style="width: 200px; float: right; padding-right: 10px;">
						<input type="text" name="payment_amount_<?=$i;?>" id="payment_amount_<?=$i;?>" class="form-control" placeholder="Payment Amount" 
					data-parsley-required="#true" data-parsley-required-message="Please enter Payment Amount"
					data-parsley-trigger="change" data-parsley-pattern="[0-9]*(\.?[0-9]{1,2}$)?" data-parsley-error-message="Please enter Price (positive numeric, Decimal 2 value)">
					</div>
					<div style="clear: both;"></div>
				</td>				
			</tr>
			<tr>
				<td>Payment Date</td>
				<td>
					<div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
						<input type="text" name="payment_date_<?=$i;?>" id="payment_date_<?=$i;?>" class="form-control" placeholder="Payment Date">
						<span class="input-group-btn">
						<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
						</span>
					</div>
				</td>				
			</tr>	
			<? } ?>		
			</table>
		<br><br>
		
		<div class="form-group">
			<div class="col-md-offset-3 col-md-9">	
				<input type="hidden" name="hidbtnsubmit_order_pay" id="hidbtnsubmit_order_pay">
				<input type="hidden" name="data_count" id="data_count" value="<?=$data_count;?>">
				<button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="return validateForm()">Update</button>
			</div>
		</div><br><br>
		</form>	
	</div>
	</div>
	</div>
</div>	
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.7.2/parsley.min.js"></script>
<script>
$('.date-picker1').datepicker({	
	orientation: "left",
	endDate: "<?php echo date('d-m-Y');?>",
	autoclose: true
});
function validateForm(){
	var data_count = $('#data_count').val();
	var validate = 0;
	for(var i=1; i<=data_count; i++){
		var order_price = $('#order_price_'+i).val();
		var checked = $('#payment_partial_'+i).is(':checked');
		var payment_amount = $('#payment_amount_'+i).val();
		var payment_date = $('#payment_date_'+i).val();
		
		if(payment_amount == ''){
			alert('Please enter Payment Amount for order ('+i+')');
			$('#payment_amount_'+i).focus();
			return false;
		}else if(payment_amount == 0){
			alert('Please enter Price (positive numeric, Decimal 2 value) for order ('+i+')');
			$('#payment_amount_'+i).focus();
			return false;
		}
		else if(payment_amount < order_price && checked == false){
			alert('Please check if Partial Payment received for order ('+i+')');
			$('#order_price_'+i).focus();
			return false;
		}
		if(payment_date == ''){
			alert('Please select Payment Date for order ('+i+')');
			$('#payment_date_'+i).focus();
			return false;
		}
	}
	if(validate == 0){
		$('#order_update').submit();
	}
}
</script>