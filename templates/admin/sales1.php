<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);
?>
<!-- END HEADER -->
<?php

if(isset($_POST['hidbtnsubmit']))
{
	$id=$_POST['id'];
	$user_type="SalesPerson";	
	$userObj->updateLocalUserDetails($user_type, $id);
	
	$working_detail = $userObj->getLocalUserWorkingAreaDetails($id);
	if($working_detail == 0)
		$userObj->addLocalUserWorkingAreaDetails($id);
	else
		$userObj->updateLocalUserWorkingAreaDetails($id);	
	
	
	$other_detail = $userObj->getLocalUserOtherDetails($id);
	if($other_detail == 0)
		$userObj->addLocalUserOtherDetails($id);
	else
		$userObj->updateLocalUserOtherDetails($id);
	
	$userObj->updateCommonUserDetails($id);
	
	echo '<script>location.href="sales.php";</script>';
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "SalesPerson";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- /.modal -->
			<h3 class="page-title">Regional Head</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="sales.php">Regional Head</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Regional Head</a> 
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Regional Head
							</div>
							 <a href="updatepassword.php?id=<?php echo base64_encode($_GET['id']);?>" class="btn btn-sm btn-default pull-right mt5">							
                                Change Password
                              </a>
						</div>
						<div class="portlet-body">
						
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						
					    <?php
						$id			= $_GET['id'];
						$userObj 	= 	new userManager($con,$conmain);
						$user_details = $userObj->getLocalUserDetails($id);
						$other_details = $userObj->getLocalUserOtherDetails($id);
						$working_area_details = $userObj->getLocalUserWorkingAreaDetails($id);
						if($working_area_details != 0)
						{
							$row1 = array_merge($user_details,$working_area_details);//print"<pre>";print_r($row1);
							if($row1['state_ids'] == '')
								$row1['state_ids'] = $user_details['state'];
							if($row1['city_ids'] == '')
								$row1['city_ids'] = $user_details['city'];
						}
						else{
							$row1 = $user_details;
							$row1['state_ids'] = $user_details['state'];
							$row1['city_ids'] = $user_details['city'];
						}											
					
						
						if($other_details != 0)
						$row1 = array_merge($row1,$other_details);//print"<pre>";print_r($row1);
						?>                       

					<form  name="updateform" id="updateform" class="form-horizontal" role="form" data-parsley-validate="" action="" method="post">
						
						<?php $page_to_update = 'sales_person'; include "userUpdateCommEle.php";	//form common element file with javascript validation ?>    
						<div class="form-group">
						  <label class="col-md-3">Office Phone No.:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Office Phone No."
							data-parsley-trigger="change"					
							data-parsley-minlength="10"
							data-parsley-maxlength="15"
							data-parsley-maxlength-message="Only 15 characters are allowed"
							data-parsley-pattern="^(?!\s)[0-9]*$"
							data-parsley-pattern-message="Please enter numbers only"
							name="phone_no" value="<?=$row1['phone_no'];?>" 
							class="form-control">
						  </div>
						</div>
						<div class="form-group">
							<label class="col-md-3"><b>Bank Details</b></label>
						</div>						
						 <div class="form-group">
							<label class="col-md-3">Account Name:</label>
							<div class="col-md-4">
								<input type="text" 
								placeholder="Account Name"
								data-parsley-trigger="change"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								name="accname" 
								value="<?=$row1['accname'];?>" 
								class="form-control">
							</div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Account Number:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Account Number"
							data-parsley-trigger="change"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="accno" class="form-control" value="<?=$row1['accno'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Name:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Bank Name"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="bank_name" class="form-control" value="<?=$row1['bank_name'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Branch Name:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Branch Name"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="accbrnm" class="form-control" value="<?=$row1['accbrnm'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">IFSC Code:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter IFSC Code"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="accifsc" class="form-control" value="<?=$row1['accifsc'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">GSTIN:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter GSTIN"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="gst_number_sss" class="form-control" value="<?=$row1['gst_number_sss'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Status:</label>
						  <div class="col-md-4">
						  <div class="input-group">					
								<select name="user_status" id="user_status" class="form-control">
									<option value="Active" <?php if($row1['user_status'] == 'Active') echo "selected";?>>Active</option>
									<option value="Inactive" <?php if($row1['user_status'] == 'Inactive') echo "selected";?>>Inactive</option>
								</select>
							</div>
						  </div>
						</div><!-- /.form-group -->	
						<div class="form-group">
							<div class="col-md-4 col-md-offset-3">
								<input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
								<input type="hidden" name="hidAction" id="hidAction" value="sales1.php">
								<input type="hidden" name="id" id="id" value="<?=$row1['id'];?>">
								<? if($_SESSION[SESSION_PREFIX."user_type"]!="Distributor") { ?>
								<button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
								<? } ?>
								<a href="sales.php" class="btn btn-primary">Cancel</a>
							</div>
						</div><!-- /.form-group -->
					</form>  
				   <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
					<div class="modal-dialog" style="width:300px;">
						<div class="modal-content">
							<div class="modal-body">
								<p>
								<h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4>
								</p>                     
							  <center><a href="sales_delete.php?id=<?php echo $row1['id']?>" ><button type="button" class="btn btn-success">Yes</button></a>
							  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
							  </center>
							</div>    
						</div>
					</div>
					</div>  
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>