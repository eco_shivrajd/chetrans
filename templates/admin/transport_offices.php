<!-- BEGIN HEADER -->
<?php header("Cache-Control: no-store, must-revalidate, max-age=0");
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
include "../includes/grid_header.php";
include "../includes/commonManage.php";
include "../includes/transportManage.php";
$tObj 		= 	new transportManager($con,$conmain);
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageTransport"; $activeMenu = "TransportOffices";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<h3 class="page-title">Transport Offices</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li><i class="fa fa-home"></i>
					<a href="#">Transport Offices</a></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
						
							<div class="caption">Transport Offices Listing</div>
							
							<? if($_SESSION[SESSION_PREFIX."user_type"]=="Admin")  { ?>
								<a href="transport_office-add.php" class="btn btn-sm btn-default pull-right mt5">Add Transport Office</a>
							<? } ?>
							
                            <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_2">
								<thead>
									<tr>
										<th>
											 Transport Office Name
										</th>
										<th>
											 Transport Type
										</th>
										<th>
											Region
										</th>
										<th>
											City
										</th>
										<th>
											State
										</th>
										<th>
											Status
										</th>
										<th>
										  Action
										</th>
									</tr>
								</thead>
							<tbody>
							<?php
							$result1 = $tObj->getAllTransportOffices();
							if($result1 != 0){
								$row_count = mysqli_num_rows($result1);
								while($row = mysqli_fetch_array($result1))
								{	
									echo '<tr class="odd gradeX">
									<td>
										<a href="transport_office1.php?id='.$row['id'].'">'.fnStringToHTML($row['trans_off_name']).'</a>
									</td>';
									echo '<td>'.$row['transport_name'].'</td>';
									echo '<td>'.$row['region_name'].'</td>';
									echo '<td>'.$row['city_name'].'</td>';
									echo '<td>'.$row['state_name'].'</td>';
									echo '<td>'.$row['status'].'</td>';
									if($row_count > 1){										
										echo '<td>
											<a href="manageuser.php?utype=Transport Office&id='.$row['id'].'">Delete</a>
										</td>';
									}else{
										echo '<td>-</td>';
									}
									echo '</tr>';
								} 
							} ?>
	
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>