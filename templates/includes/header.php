<?php
include ("../../includes/config.php");
include ("../includes/common.php");
if(!isset($_SESSION[SESSION_PREFIX."user_id"])) {	
	echo '<script>location.href="../login.php";</script>';
	exit;
}
$condn="";
$condnsearch="";
$selperiod=$_REQUEST['selTest'];
$order_status=$_REQUEST['order_status'];
//echo $selperiod;
//echo "--string--";
//echo $order_status;
switch($selperiod){
	case 1:
		$condnsearch=" AND yearweek(o.order_date) = yearweek(curdate())";
	break;
	case 2:
		$condnsearch=" AND date_format(o.order_date, '%m')=date_format(now(), '%m')";
	break;
	case 3:
		if($_REQUEST['todate']!="")
			$todat=$_REQUEST['todate'];
		else
			$todat = date("d-m-Y");
	
		$condnsearch=" AND (date_format(o.order_date, '%Y-%m-%d') >= STR_TO_DATE('".$_REQUEST['frmdate']."','%d-%m-%Y') AND date_format(o.order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
	break;
	case 4:
		$condnsearch=" AND DATE_FORMAT(o.order_date,'%Y-%m-%d')=DATE(NOW())";
	break;
	case 0:
		$condnsearch="";
	break;
	default:
		$condnsearch="";
	break;
}

switch ($order_status) {
	    case '1':
		$order_status_search ="od.order_status =1";
		break;
	    case '2':
		$order_status_search ='od.order_status =2';
		break;
		case '3':
		$order_status_search ='od.order_status =3';
		break;
		case '4':
		$order_status_search ='od.order_status =4';
		break;
		case '6':
		$order_status_search ='od.order_status =6';
		break;
		case '8':
		$order_status_search ='od.order_status =8';
		break;	
		case 0:
			$order_status_search="1=1";
		break;
		default:
			$order_status_search="1=1";
			break;
}

if(isset($_SESSION[SESSION_PREFIX."firstname"]))
{
?>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<style>
g text:last-child {font-size :15px;}
</style>
<meta charset="utf-8"/>
<title><?=SITETITLE;?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css"></script>
<!-- BEGIN THEME STYLES -->
<link href="../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="../../assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
<!-- amCharts javascript sources -->
		<script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
		<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/pie.js"></script>
		<script type="text/javascript" src="../includes/js/common.js"></script>
		<?php
		if (strpos($_SERVER['SCRIPT_NAME'], 'index.php') !== false)
		{
						

 $sqlregionwise = "SELECT o.order_no, o.total_cost, od.product_unit_cost, s.address, ts.suburbnm, od.order_status, o.order_date FROM tbl_orders o  LEFT JOIN tbl_order_details od ON o.id = od.order_id  LEFT JOIN tbl_shops s ON o.shop_id = s.id LEFT JOIN tbl_surb ts ON ts.id = s.suburbid WHERE ".$order_status_search." ".$condnsearch." GROUP BY s.suburbid";
//echo $sqlregionwise;

$resultregionwise = mysqli_query($con,$sqlregionwise);
$rowsregionwise = array();
while ($rowrw = mysqli_fetch_array ($resultregionwise)){
     $rowsregionwise[] = array(
    "category" => $rowrw['suburbnm'],
   "column-1" => $rowrw['total_cost'] 
  );
  }
  $txtnmmm="Region Wise Trend";
  $regionwisetrend= json_encode($rowsregionwise);

$sqlproducttrend="SELECT od.id, od.order_id, 				
od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,od.product_quantity,od.product_unit_cost, od.p_cost_cgst_sgst, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, od.order_status, o.order_date FROM `tbl_order_details` AS od
LEFT JOIN tbl_orders o ON od.order_id = o.id WHERE ".$order_status_search." ".$condnsearch." GROUP BY od.product_id";

//echo $sqlproducttrend;

$resultproducttrend = mysqli_query($con,$sqlproducttrend);
$rowsproduct = array();
while ($rowpr = mysqli_fetch_array ($resultproducttrend)){
     $rowsproduct[] = array(
    "category" => $rowpr['product_name'],
   "column-1" => $rowpr['p_cost_cgst_sgst'] 
  );
  }
  $txtnmm="Product Wise Trend";
  $producttrend= json_encode($rowsproduct);

$sqldeliverytrend="SELECT od.id, od.order_id,od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,od.product_quantity,od.product_unit_cost, od.p_cost_cgst_sgst, od.product_cgst, od.product_sgst,od.p_cost_cgst_sgst FROM `tbl_order_details` AS od LEFT JOIN tbl_orders o ON od.order_id = o.id WHERE od.order_status = '4' ".$condnsearch." GROUP BY od.product_id";
$resultdeliverytrend = mysqli_query($con,$sqldeliverytrend);
$rowsdelivery = array();
while ($rowdr = mysqli_fetch_array ($resultdeliverytrend)){
     $rowsdelivery[] = array(
    "category" => $rowdr['product_name'],
   "column-1" => $rowdr['p_cost_cgst_sgst'] 
  );
  }
  $txtnmmq="Product Wise Trend";
  $deliverytrend= json_encode($rowsdelivery);

?>
			<!-- amCharts javascript code -->
		<script type="text/javascript">
			var chart = AmCharts.makeChart("chartdiv",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start",
						"labelRotation": 45,
						"labelsEnabled":false
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Region Wise Trend",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "₹"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": ""
						}
					],
					"dataProvider": <?php echo $regionwisetrend;?>
				}
			);			
		</script>		
		<!-- amCharts javascript code -->
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv1",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start",
						"labelRotation": 45,
						"labelsEnabled":false
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"fillColors": "#12D812",
							"id": "AmGraph-1",
							"title": "Farmer Wise Sales in ₹",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "₹"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": " "
						}
					],
					//"dataProvider": <?php //echo $stockistarr;?>
					"dataProvider": <?php echo $superstockistarr;?>
				}
			);
		</script>
		<!-- amCharts javascript code -->
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv2",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start",
						"labelRotation": 45,
						"labelsEnabled":false
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							//"title": "<?php echo $txtnm;?>",
							"title": "Delivery Wise Trend",
							
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "₹"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": " "
						}
					],
					"dataProvider": <?php echo $deliverytrend;?>
				}
			);
		</script>
		<script type="text/javascript">
			AmCharts.makeChart("chartdiv3",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start",
						"labelRotation": 45,
						"labelsEnabled":false
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Product Wise Trend",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "₹" 
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": false,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": " "
						}
					],
						"dataProvider":<?php echo $producttrend;?>
				}
			);
		</script>		
		<style>
		.fa.fa-inr.fa-6 {font-size: 30px;}
		.amcharts-chart-div > a {display: none !important;}
		</style>
		<?php 
		}// index page...
		?>
</head>
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="./index.php">
			<img src="<?=PORTALLOGOPATH;?>" alt="logo" class="logo-default"/>
			</a>
			<div class="menu-toggler sidebar-toggler hide">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
			
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

					<span class="username ">
					<?php echo "Welcome "."<b>".$_SESSION[SESSION_PREFIX.'firstname']."</b>";?> </span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
					 
                           <li>
							<a href="profile.php">
							<i class="icon-user"></i> My Profile </a>
						</li>
						  <li>
							<a href="changepassword.php">
							<i class="icon-lock"></i>Change Password </a>
						</li>
					 
						<li>
                            <?php echo '<a href="../logout.php"><i class="icon-key"></i> Logout </a>';?>
						</li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<?php
}
else
{	
header("location:../login.php");
}
?>