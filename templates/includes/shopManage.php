<?php
/***********************************************************
 * File Name	: shopManage.php
 ************************************************************/	

class shopManager
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);
	}	
	public function checkShopAccess($id) {
		$where_clause = "";
		if($_SESSION[SESSION_PREFIX.'user_type'] == 'Superstockist')
		{
			$user_id		= $_SESSION[SESSION_PREFIX.'user_id'];	
			$where_clause = " AND (sstockist_id =".$user_id." OR stockist_id IN (SELECT id FROM tbl_user WHERE external_id = ".$user_id." ))";
		}
		else if($_SESSION[SESSION_PREFIX.'user_type'] == 'Distributor')
		{
			$user_id		= $_SESSION[SESSION_PREFIX.'user_id'];	
			$where_clause = " AND stockist_id =".$user_id;
		}
		$sql1="SELECT `id`	FROM tbl_shops WHERE id=".$id.$where_clause;
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return 1;		
		}else
			return $row_count;		
	}	
	public function getAllShopsByUser() {
		$where_clause = "";
		if($_SESSION[SESSION_PREFIX.'user_type'] == 'Superstockist')
		{
			$user_id		= $_SESSION[SESSION_PREFIX.'user_id'];	
			$where_clause = " and  (sstockist_id =".$user_id." OR stockist_id IN (SELECT id FROM tbl_user WHERE external_id = ".$user_id." ))";
		}
		else if($_SESSION[SESSION_PREFIX.'user_type'] == 'Distributor')
		{
			$user_id		= $_SESSION[SESSION_PREFIX.'user_id'];	
			$where_clause = " and  stockist_id =".$user_id;
		}
		$sql1="SELECT `id`, `name`, `address`, `city`, `state`, `contact_person`, `mobile`, `shop_added_by`, `lat`, 
		`lon`, `contact_person_other`, `mobile_number_other`, `gst_number`, `suburbid`, `closedday`, `opentime`, 
		`closetime`, `latitude`, `longitude`, `subarea_id`,`sstockist_id`,`stockist_id`, `status`
		FROM tbl_shops  where isdeleted!='1' ".$where_clause;
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}	
	//shivraj
	public function getAllShopsByfilter() {
		$where_clause = " ";		
		if(!empty($_GET['state'])){
			$where_clause .=" and state=".$_GET['state'].' ';
			if($_GET['city'] != ""){
				$where_clause .=" and city=".$_GET['city'].' ';
				if($_GET['area'] != ""){
					$where_clause .=" and suburbid = ".$_GET['area'].' ';
					if($_GET['subarea'] != ""){
						$where_clause .=" and subarea_id = ".$_GET['subarea'].' ';
					}
				}
			}
		}
		$sql1="SELECT `id`, `name`, `address`, `city`, `state`, `contact_person`, `mobile`, `shop_added_by`, `lat`, 
		`lon`, `contact_person_other`, `mobile_number_other`, `gst_number`, `suburbid`, `closedday`, `opentime`, 
		`closetime`, `latitude`, `longitude`, `subarea_id`,`sstockist_id`,`stockist_id`, `status`
		FROM tbl_shops WHERE isdeleted!='1' AND shop_status = 'Active' ".$where_clause. " order by name";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}	
	
	public function getShopDetails($id) {		
		$sql1="SELECT `id`, `name`, `address`, 
		`city`, (SELECT name FROM tbl_city WHERE id = tbl_shops.city) AS city_name,
		`state`, (SELECT name FROM tbl_state WHERE id = tbl_shops.state) AS state_name,
		`contact_person`, `mobile`, `shop_added_by`, `lat`, `lon`, 
		`contact_person_other`, `mobile_number_other`, `gst_number`, 
		`suburbid`, `closedday`, `opentime`, `closetime`, `latitude`, 
		`longitude`, `subarea_id`, `sstockist_id`, `stockist_id`, 
		`status`, `status_seen_log`, `isdeleted`, `shop_type_id`, 
		`bank_acc_name`, `bank_acc_no`, `bank_b_name`,`bank_name`,`bank_ifsc`, 
		`shop_status`
		FROM tbl_shops WHERE id = '$id' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}	
	public function getShopAssignToUsers($id) {		
		$sql1="SELECT `user_id`
		FROM tbl_shop_assignedto_users WHERE shop_id = '$id' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}	
	public function addShopDetails() {
		extract ($_POST);
		$name=fnEncodeString($name);
		$address=fnEncodeString($address);		
		$contact_person= fnEncodeString($contact_person);
		$shop_added_by=$_SESSION[SESSION_PREFIX.'user_id'];

		//var_dump($_POST);
		//exit();
	
		$fields = '';
		$values = ''; 		
		if($shop_type_id != '')
		{
			$fields.= ",`shop_type_id`";			
			$values.= ",'".$shop_type_id."'";
		}
		if($area != '')
		{
			$fields.= ",`suburbid`";			
			$values.= ",'".$area."'";
		}		
		if($contact_person_other != '')
		{
			$fields.= ",`contact_person_other`";
			$values.= ",'".fnEncodeString($contact_person_other)."'";
		}
		if($mobile_number_other != '')
		{
			$fields.= ",`mobile_number_other`";
			$values.= ",'".fnEncodeString($mobile_number_other)."'";
		}
		if($gst_number != '')
		{
			$fields.= ",`gst_number`";
			$values.= ",'".fnEncodeString($gst_number)."'";
		}
		if($closedday != '')
		{
			$fields.= ",`closedday`";
			$values.= ",'".$closedday."'";
		}
		if($opentime != '')
		{
			$fields.= ",`opentime`";
			$values.= ",'".$opentime."'";
		}
		if($closetime != '')
		{
			$fields.= ",`closetime`";
			$values.= ",'".$closetime."'";
		}
		if($latitude != '')
		{
			$fields.= ",`latitude`";
			$values.= ",'".fnEncodeString($latitude)."'";
		}
		if($longitude != '')
		{
			$fields.= ",`longitude`";
			$values.= ",'".fnEncodeString($longitude)."'";
		}
		if($bank_acc_name != '')
		{
			$fields.= ",`bank_acc_name`";
			$values.= ",'".fnEncodeString($bank_acc_name)."'";
		}
		if($bank_acc_no != '')
		{
			$fields.= ",`bank_acc_no`";
			$values.= ",'".fnEncodeString($bank_acc_no)."'";
		}
		if($bank_b_name != '')
		{
			$fields.= ",`bank_b_name`";
			$values.= ",'".fnEncodeString($bank_b_name)."'";
		}
		if($bank_ifsc != '')
		{
			$fields.= ",`bank_ifsc`";
			$values.= ",'".fnEncodeString($bank_ifsc)."'";
		}
		if($bank_name != '')
		{
			$fields.= ",`bank_name`";
			$values.= ",'".fnEncodeString($bank_name)."'";
		}
		/*if($gstnumber != '')
		{
			$fields.= ",`gstnumber`";
			$values.= ",'".fnEncodeString($gstnumber)."'";
		}*/

		if($shop_status != '')
		{
			$fields.= ",`shop_status`";
			$values.= ",'".fnEncodeString($shop_status)."'";
		}
		$status = 0;//shop added through web
		$sql = "INSERT INTO tbl_shops (`name`,`address`,`city`,`state`,`contact_person`,`mobile`,`shop_added_by`,`status` $fields) 
		VALUES('".$name."','".$address."','".$city."','".$state."','".$contact_person."','".$mobile."','".$shop_added_by."',$status $values)";

		mysqli_query($this->local_connection,$sql);
		$shopid=mysqli_insert_id($this->local_connection); 
		$this->commonObj->log_add_record('tbl_shops',$shopid,$sql);	
		//$this->addShopAssignToUsers($shopid,$assign);
	}
	public function addShopAssignToUsers($shopid,$userid) {		
		$sql = "INSERT INTO tbl_shop_assignedto_users (`shop_id`,`user_id`) 
		VALUES('".$shopid."','".$userid."')";

		mysqli_query($this->local_connection,$sql);	
		$this->commonObj->log_add_record('tbl_shop_assignedto_users',$shopid,$sql);	
	}
	public function updateShopDetails($id) {
		extract ($_POST);
		$name=fnEncodeString($name);
		$address=fnEncodeString($address);		
		$contact_person= fnEncodeString($contact_person);
		$shop_added_by=$_SESSION[SESSION_PREFIX.'user_id'];
		
		$values = ''; 
		if($shop_type_id != '')
		{		
			$values.= ", `shop_type_id` = '".$shop_type_id."'";
		}
		if($area != '')
		{		
			$values.= ", `suburbid`='".$area."'";
		}
		
		if($contact_person_other != '')
		{
			$values.= ", `contact_person_other`='".fnEncodeString($contact_person_other)."'";
		}else{
			$values.= ", `contact_person_other`=''";
		}
		if($mobile_number_other != '')
		{
			$values.= ", `mobile_number_other`='".fnEncodeString($mobile_number_other)."'";
		}else{
			$values.= ", `mobile_number_other`=''";
		}
		if($gst_number != '')
		{
			$values.= ", `gst_number`='".fnEncodeString($gst_number)."'";
		}else
		{
			$values.= ", `gst_number`=''";
		}
		if($closedday != '')
		{
			$values.= ", `closedday`='".$closedday."'";
		}else
		{
			$values.= ", `closedday`=''";
		}
		if($opentime != '')
		{
			$values.= ", `opentime`='".$opentime."'";
		}
		if($closetime != '')
		{
			$values.= ", `closetime`='".$closetime."'";
		}
		if($latitude != '')
		{
			$values.= ", `latitude`='".fnEncodeString($latitude)."'";
		}
		else
		{
			$values.= ", `latitude`=''";
		}
		if($longitude != '')
		{			
			$values.= ", `longitude`='".fnEncodeString($longitude)."'";
		}
		else
		{			
			$values.= ", `longitude`=''";
		}
		if($bank_acc_name != '')
		{
			$values.= ", `bank_acc_name` = '".fnEncodeString($bank_acc_name)."'";
		}
		if($bank_acc_no != '')
		{
			$values.= ", `bank_acc_no` = '".fnEncodeString($bank_acc_no)."'";
		}
		if($bank_b_name != '')
		{
			$values.= ", `bank_b_name` = '".fnEncodeString($bank_b_name)."'";
		}
		if($bank_name != '')
		{
			$values.= ", `bank_name` = '".fnEncodeString($bank_name)."'";
		}
		if($bank_ifsc != '')
		{
			$values.= ", `bank_ifsc` = '".fnEncodeString($bank_ifsc)."'";
		}
		if($shop_status != '')
		{
			$values.= ", `shop_status` = '".fnEncodeString($shop_status)."'";
		}
		$update_sql="UPDATE tbl_shops SET name='$name',address='$address',state='$state',city='$city',contact_person='$contact_person',mobile='$mobile' $values where id='$id'";		
		mysqli_query($this->local_connection,$update_sql);
		$this->commonObj->log_update_record('tbl_shops',$id,$update_sql);
		//$update_user_assign_sql="UPDATE tbl_shop_assignedto_users SET user_id='$assign' where shop_id='$id'";		
		//mysqli_query($this->local_connection,$update_user_assign_sql);
		//$this->commonObj->log_update_record('tbl_shop_assignedto_users',$id,$update_user_assign_sql);
	}
	public function getShopStatusNLog($id) {		
		$sql1="SELECT `status`, `status_seen_log` 
		FROM tbl_shops WHERE id = '$id' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function updateViewStatus($id) {
		$shop_data = $this->getShopStatusNLog($id);
		$current_log = $shop_data['status_seen_log'];//json format
		$current_log_array = json_decode($current_log);
		$current_log_array = (array)$current_log_array;
		$log_array = array();
		$status = '';
		if($shop_data['status'] != 0 && $shop_data['status'] != NULL){//Shop added by Sales Person
			switch($_SESSION[SESSION_PREFIX.'user_type']){
				case "Admin":											
					if($shop_data['status'] != 4)//shop details not seen by Admin
					{
						$status = 4;	
						$log_array['date'] = date('d-m-Y');
						$log_array['by'] = $_SESSION[SESSION_PREFIX.'user_type']."_4";
						$log_array['by_id'] = $_SESSION[SESSION_PREFIX.'user_id'];
					}
				break;
				case "Superstockist":											
					if($shop_data['status'] != 3)//shop details not seen by Superstockist
					{	
						$status = 3;
						$log_array['date'] = date('d-m-Y');
						$log_array['by'] = $_SESSION[SESSION_PREFIX.'user_type']."_3";
						$log_array['by_id'] = $_SESSION[SESSION_PREFIX.'user_id'];
					}
				break;
				case "Distributor":
					if($shop_data['status'] != 2)//shop details not seen by Distributor
					{							
						$status = 2;	
						$log_array['date'] = date('d-m-Y');
						$log_array['by'] = $_SESSION[SESSION_PREFIX.'user_type']."_2";
						$log_array['by_id'] = $_SESSION[SESSION_PREFIX.'user_id'];
					}
				break;
			}
			if($status != '')
			{
				if($shop_data['status_seen_log'] != '' && !in_array($status,array_values($current_log_array['status']))){
					$current_log_array['status'] = (array)$current_log_array['status'];
					$current_log_array['log'] = (array)$current_log_array['log'];
					$current_log_array['status'][] = $status;
					$current_log_array['log'][] = $log_array;
					$update_json = json_encode($current_log_array);
					$update_sql="UPDATE tbl_shops SET status='$status',status_seen_log='$update_json' where id='$id'";	
					mysqli_query($this->local_connection,$update_sql);	
				}else{
					$new_log_array['status'][] = $status;
					$new_log_array['log'][] = $log_array;
					$update_json = json_encode($new_log_array);
					$update_sql="UPDATE tbl_shops SET status='$status',status_seen_log='$update_json' where id='$id'";		
					mysqli_query($this->local_connection,$update_sql);	
				}				
			}
		}		
	}
	public function getAllShopTypes() {		
		echo $sql="SELECT `id`, `shop_type`, `status`
		FROM tbl_shop_type  where status='0' ";
		$result = mysqli_query($this->local_connection,$sql);
		$row_count = mysqli_num_rows($result);
		if($row_count > 0){	
			return $result;		
		}else
			return $row_count;		
	}
	public function getAllShops() {
		$where_clause = "";		
		if(!empty($_GET['state'])){
			$where_clause .=" and state=".$_GET['state'].' ';
			if($_GET['city'] != ""){
				$where_clause .=" and city=".$_GET['city'].' ';
				if($_GET['area'] != ""){
					$where_clause .=" and suburbid = ".$_GET['area'].' ';
					if($_GET['subarea'] != ""){
						$where_clause .=" and subarea_id = ".$_GET['subarea'].' ';
					}
				}
			}
		}
		if($where_clause != '')
			$where_clause = ' WHERE '.$where_clause;
		
		$sql1="SELECT `id`, `name`, `address`, `city`, `state`, `contact_person`, `mobile`, `shop_added_by`, `lat`, 
		`lon`, `contact_person_other`, `mobile_number_other`, `gst_number`, `suburbid`, `closedday`, `opentime`, 
		`closetime`, `latitude`, `longitude`
		FROM tbl_shops ".$where_clause. " order by name";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}		
}
?>