<?php
/***********************************************************
 * File Name	: userManage.php
 ************************************************************/	
include "../includes/commonManage.php";	
class userManager 
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);
	}
	public function getAllLocalUserDetails($user_type,$external_id=''){
		$where_clause = ''; 
		if($user_type == 'SalesPerson' && $_SESSION[SESSION_PREFIX.'user_type'] == "Superstockist" && $external_id != ''){
			$where_clause = " AND (sstockist_id = ". $external_id.")";
		}else if($external_id != '')
		{			
			$where_clause = " AND (external_id IN (". $external_id.") OR external_id LIKE ('%,". $external_id."%'))";
		}
		$sql1="SELECT `id`, `external_id`, `surname`, `firstname`, `username`, 
		`pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, 
		`reset_key`, `suburbid`, `is_default_user`, `sstockist_id`, `user_status`
		FROM tbl_user where user_type = '".$user_type."' AND tbl_user.isdeleted!='1' $where_clause order by is_default_user desc, firstname asc";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}
	public function getLocalUserDetailsByUserType($user_type,$external_id='') {
		$where_clause = '';
		if($external_id != '')
		{
			$where_clause = " AND external_id = ". $external_id;
		}
		$sql1="SELECT `tbl_user.id` AS id, `external_id`, `surname`, `firstname`, `username`, 
		`pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, 
		`reset_key`, `suburbid`, `is_default_user`, `sstockist_id`
		FROM tbl_user where user_type = '".$user_type."' AND tbl_user.isdeleted!='1'  $where_clause order by is_default_user desc, firstname asc";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}
	public function getLocalUserDetails($user_id) {
		$sql1="SELECT `id`, `external_id`, `surname`, `firstname`, `username`, 
		`pwd`, `user_type`, `address`, `city`, 
		`state`,  (SELECT name FROM tbl_state WHERE id = tbl_user.state) AS state_name,
		`mobile`,`office_ph_no`,`email`, `reset_key`, `suburbid`, `is_default_user`, `sstockist_id`, `gst_number_sss`,		
		(SELECT suburbnm FROM tbl_surb WHERE id = wa.suburb_ids) AS suburbnm, user_status,
		gst_number_sss
		FROM 
		tbl_user 
		LEFT JOIN tbl_user_working_area AS wa ON wa.user_id = id
		where id = '".$user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0)
		{	
			return $row = mysqli_fetch_assoc($result1);		
		}
		else
			return $row_count;		
	}
	/*public function getLocalUserBankDetails($user_id) {
		$sql1="SELECT `id`, `userid`, `accno`, `accbrnm`, `accifsc`
		FROM tbl_sp_bank where userid = '".$user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}*/
	public function getLocalUserWorkingRegionDetails($user_id) {
		$sql1="SELECT `user_id`, `state_ids`, `city_ids`, `suburb_ids`, `subarea_ids`,
		(SELECT name FROM tbl_state WHERE id = tbl_user_working_area.state_ids) AS state_name,
		(SELECT name FROM tbl_city WHERE id = tbl_user_working_area.city_ids) AS city_name,
		(SELECT  suburbnm FROM tbl_surb WHERE id = tbl_user_working_area.suburb_ids) AS region_name
		FROM `tbl_user_working_area`
		where user_id = '".$user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	
	public function getLocalUserWorkingAreaDetails($user_id){
		$sql1="SELECT `state_ids`, `city_ids`, `suburb_ids`, `subarea_ids` FROM tbl_user_working_area where user_id = '".$user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;	
	}
	public function getCommonUserDetails($user_id) {
		$sql1="SELECT `uid`, `emailaddress`, `passwd`, `username`, `level`, `reset_key`, `id`, `address`, `state`, `city`, `mobile`, `firstname`, `suburbid` 
		FROM tbl_users where id = '".$user_id."'";
		$result1 = mysqli_query($this->common_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function getCommonUserDetailsByUsername($username) {
		$sql1="SELECT `uid`, `emailaddress`, `passwd`, `username`, `level`, `reset_key`, `id`, `address`, `state`, `city`, `mobile`, `firstname`, `suburbid` 
		FROM tbl_users where username='".$username."'";
		$result1 = mysqli_query($this->common_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;			
	}
	public function getCommonUserCompanyDetails($user_id) {
		$sql1="SELECT `id`, `userid`, `companyid` FROM tbl_user_company where userid='".$user_id."' AND companyid='".COMPID."'";
		$result1 = mysqli_query($this->common_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;
	}
	public function addLocalUserDetails($user_type) {
		extract ($_POST);
		$external_id =	$_SESSION[SESSION_PREFIX.'user_id'];	
		
		if(($user_type == 'Distributor' OR $user_type == 'SalesPerson') && $assign != "")
		{
			$external_id = $assign;
			if($user_type == 'SalesPerson'){
				if(count($assign) > 1){
					$external_id = implode(',',$assign);
				}else{
					$external_id = $assign[0];
				}
				if(gettype($_POST['assign']) == string)
					$external_id = $assign;
			}
		}
		
		$surname	=	"";
		$name		=	fnEncodeString(trim($firstname));
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$email		=	fnEncodeString($email);
		$mobile		= 	fnEncodeString($mobile);		
		$address	= 	fnEncodeString($address);		
		
		$fields = '';
		$values = ''; 
		if($user_type == 'SalesPerson' && $cmbSuperStockist != ''){			
			$fields.= ",`sstockist_id`";
			$values.= ",".$cmbSuperStockist;
		}
		if($email != '')
		{
			$fields.= ",`email`";
			$values.= ",'".$email."'";
		}
		if($mobile != '')
		{
			$fields.= ",`mobile`";
			$values.= ",'".$mobile."'";
		}
		if($address != '')
		{
			$fields.= ",`address`";
			$values.= ",'".$address."'";
		}
		if($state != '')
		{
			$fields.= ",`state`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city`";
			$values.= ",'".$city."'";
		}
		if($gstnumber != '')
		{
			$fields.= ",`gst_number_sss`";
			$values.= ",'".$gstnumber."'";
		}
		if($user_status != '')
		{
			$fields.= ",`user_status`";
			$values.= ",'".fnEncodeString($user_status)."'";
		}
		$user_sql = "INSERT INTO tbl_user (`external_id`,`surname`,`firstname`,`username`,`pwd`,`user_type` $fields) 
		VALUES('".$external_id."','".$surname."','".$name."','".$username."','".$password."','".$user_type."' $values)";
		mysqli_query($this->local_connection,$user_sql);		
		$userid=mysqli_insert_id($this->local_connection); 
		$this->commonObj->log_add_record('tbl_user',$userid,$user_sql);
		
			//new added for ganeshfoods
		if($user_type == 'SalesPerson'){
			$accno		=	fnEncodeString($accno);
			$accbrnm		= 	fnEncodeString($accbrnm);		
			$accifsc	= 	fnEncodeString($accifsc);
			$user_sql1 = "INSERT INTO tbl_sp_bank (`accno`,`accbrnm`,`accifsc`,`userid`) 
			VALUES('".$accno."','".$accbrnm."','".$accifsc."','".$userid."')";
			mysqli_query($this->local_connection,$user_sql1);		
		}
		
		return $userid;
	}
	public function addLocalUserWorkingAreaDetails($user_id) {
		extract ($_POST);
		$fields = '';
		$values = ''; 
		if($state != '')
		{
			$fields.= ",`state_ids`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city_ids`";
			$values.= ",'".$city."'";
		}
		if($area != '')
		{
			$fields.= ",`suburb_ids`";
			$suburb_ids = implode(',',$area);
			$values.= ",'".$suburb_ids."'";
		}
		if($subarea != '')
		{
			$fields.= ",`subarea_ids`";
			$subarea_ids = implode(',',$subarea);
			$values.= ",'".$subarea_ids."'";
		}
		
		$user_working_area = "INSERT INTO tbl_user_working_area (`user_id` $fields) 
		VALUES('".$user_id."' $values)";
		mysqli_query($this->local_connection,$user_working_area);		
		$this->commonObj->log_add_record('tbl_user_working_area',$user_id,$user_working_area);	
	}
	public function addCommonUserDetails($user_type,$user_id) {
		extract ($_POST);
		$email		=	fnEncodeString($email);
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$fields = '';
		$values = ''; 
		if($state != '')
		{
			$fields.= ",`state`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city`";
			$values.= ",'".$city."'";
		}
		$user_sql = "INSERT INTO tbl_users (`id`, `emailaddress`, `passwd`, `username`, `level` $fields) 
		VALUES('".$user_id."','".$email."','".$password."','".$username."','".$user_type."' $values)";
		mysqli_query($this->common_connection,$user_sql);	
		return $userid=mysqli_insert_id($this->common_connection); 		
	}
	public function addCommonUserCompanyDetails($user_id) {		
		$company_sql = "INSERT INTO tbl_user_company(userid,companyid)  VALUES('".$user_id."','".COMPID."')";
		mysqli_query($this->common_connection,$company_sql);
	}
	public function sendUserCreationEmail() {
		extract ($_POST);
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$subject   = "Account Created";
		$fromMail  = FROMMAILID;		
		$headers = "";
		$headers .= "From: ".$fromMail."\r\n";
		$headers .= "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-Type: text/html;" . "\r\n";
		$message = "";
		$message .= "Hi, <br/>";
		$message .= "Your account created successfully<br/>";
		$message .= "Please find the login details below<br/>";
		$message .= "Username: $username<br/>";
		$message .= "Password: $rand<br/>";
		$message .= "<a href='".SITEURL."/templates/login.php'>Click Here</a> to Login<br/>";
		$message .= "<br/><br/>";
		$message .= "Thanks,<br/>";
		$message .= "Admin.";
		if($username != ""){
			$sent = @mail($username,$subject,$message,$headers);
		}
	}
	public function updateLocalUserDetails($user_type, $user_id) {	
		extract ($_POST);
		//echo "";print_r($_POST);exit;	
		$address= fnEncodeString($address);	
		$email	= fnEncodeString($email);
		$mobile	= fnEncodeString($mobile);			
		$values = '';
		
		$values.= " `id`= '".$user_id."'";
			
		if($address != '')
		{
			$values.= ", `address`= '".$address."'";				
		}
		if($state != '')
		{
			$values.= ", `state`= '".$state."'";
		}
		if($city != '')
		{	
			$values.= ",`city`= '".$city."'";
		}
		if($email != '')
		{
			$values.= ", `email`= '".$email."'";				
		}
		if($mobile != '')
		{
			$values.= ", `mobile`= '".$mobile."'";				
		}
		if($office_ph_no != '')
		{
			$values.= ", `office_ph_no`= '".$office_ph_no."'";				
		}
		if($user_status != '')
		{
			$values.= ", `user_status` = '".fnEncodeString($user_status)."'";
		}
		if(($user_type == 'Distributor' OR $user_type == 'SalesPerson') && $assign != "")
		{
			$external_id = $assign;
			if($user_type == 'SalesPerson'){
				if(count($assign) > 1){
					$external_id = implode(',',$assign);
				}else{
					$external_id = $assign[0];
				}	
				
			}
			$values.= ", `external_id`= '".$external_id."'";
		}
			if($firstname!="") {
				$values.= ", `firstname`= '".fnEncodeString(trim($firstname))."'";	
			}
			if($username!="") {
				$values.= ", `username`= '".fnEncodeString($username)."'";					
			}
		if($gst_number_sss!="") {
			$values.= ", `gst_number_sss`= '".($gst_number_sss)."'";					
		}
	
		if($user_type=='SalesPerson'){
			$accno		=	fnEncodeString($accno);
			$accbrnm		= 	fnEncodeString($accbrnm);		
			$accifsc	= 	fnEncodeString($accifsc);
			$update_user_sql1 = "UPDATE tbl_sp_bank SET `accno`='$accno',`accbrnm`='$accbrnm',`accifsc`='$accifsc'  WHERE userid='$user_id'";
			mysqli_query($this->local_connection,$update_user_sql1);
		}
		if($user_type=='Distributor'){
			$this->stockistremove($user_id);
		}
		
		$update_user_sql = "UPDATE tbl_user SET $values WHERE id='$user_id'"; 
		mysqli_query($this->local_connection,$update_user_sql);
		$this->commonObj->log_update_record('tbl_user',$user_id,$update_user_sql);
	}
	
	public function updateCommonUserDetails($user_id) {		
		extract ($_POST);	
		if($firstname != '')
		{
			$values.= ", `firstname`= '".fnEncodeString(trim($firstname))."'";
		}
		if($username != '')
		{
			$values.= ", `username`= '".fnEncodeString($username)."'";
		}
		if($email != '')
		{
			$values.= ", `emailaddress`= '".fnEncodeString($email)."'";
		}
		if($mobile != '')
		{
			$values.= ", `mobile`= '".$mobile."'";				
		}
		if($address != '')
		{
			$values.= ", `address`= '".fnEncodeString($address)."'";
		}
		if($state != '')
		{
			$values.= ", `state`= '".$state."'";
		}
		if($city != '')
		{	
			$values.= ",`city`= '".$city."'";
		}
		$update_user_sql = "UPDATE tbl_users SET id='$user_id' $values WHERE id='$user_id'";
		mysqli_query($this->common_connection,$update_user_sql);		
	}
	
	public function updateLocalUserWorkingAreaDetails($user_id) {
		extract ($_POST);		
		$values = ''; 
		if($state != '')
		{
			$values.= ", `state_ids`= '".$state."'";
		}
		if($city != '')
		{	
			$values.= ",`city_ids`= '".$city."'";
		}
		if($area != '')
		{
			$suburb_ids = implode(',',$area);
			$values.= ", `suburb_ids`= '".$suburb_ids."'";
		}else{
			$values.= ", `suburb_ids`= ''";
		}
		if($subarea != '')
		{			
			$subarea_ids = implode(',',$subarea);
			$values.= ",`subarea_ids`= '".$subarea_ids."'";
		}else{
			$values.= ",`subarea_ids`= ''";
		}
			
		$user_working_area = "UPDATE tbl_user_working_area SET user_id='$user_id' $values WHERE user_id='$user_id'";				
			
		mysqli_query($this->local_connection,$user_working_area);
		$this->commonObj->log_update_record('tbl_user_working_area',$user_id, $user_working_area);
	}
	function checkUserBelongTo($user_id,$external_id){
		if($external_id == $_SESSION[SESSION_PREFIX.'user_id'])
			return 0;
		else{
			$user_data = $this->getLocalUserDetails($external_id);
			if($user_data['external_id'] == $_SESSION[SESSION_PREFIX.'user_id'])
				return 0;
			else
				return 1;
		}
	}
	public function assignStockist() {	
		extract ($_POST);	
		$external_id = $assign;			
		if(count($assign) > 1){
			$external_id = implode(',',$assign);
		}else{
			$external_id = $assign[0];
		}				
		
		$values.= " `external_id`= '".$external_id."'";
		$update_user_sql = "UPDATE tbl_user SET $values WHERE id='$sales_p_id'";
		return mysqli_query($this->local_connection,$update_user_sql);
		$this->commonObj->log_update_record('tbl_user',$sales_p_id,$update_user_sql);
	}
	public function getDefaultUserDetails($user_type){
		$sql1="SELECT tbl_user_working_area.state_ids, tbl_user_working_area.city_ids, 
		tbl_user_working_area.suburb_ids, tbl_user_working_area.subarea_ids
		FROM tbl_user 
		LEFT JOIN  tbl_user_working_area ON  tbl_user_working_area.user_id = tbl_user.id
		WHERE user_type = '".$user_type."'  AND is_default_user = '1'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return mysqli_fetch_assoc($result1);		
		}else
			return $row_count;	
	}
	public function migration_sp_sstockist(){
		echo $sql1="SELECT `id`, `external_id`, `sstockist_id`
		FROM tbl_user where user_type = 'SalesPerson'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			while($row_sp = mysqli_fetch_array($result1))
			{//print"<pre>";print_R($row_sp);
				if($row_sp['sstockist_id'] == 0){
					echo "<br>#id==".$row_sp['id']."#external_id".$row_sp['external_id'];
					$stockist_array = explode(',',$row_sp['external_id']);
					$first_stockist = $stockist_array[0];
					
					echo "<br>".$sql2="SELECT `external_id`
					FROM tbl_user where id = ".$first_stockist;
					$result2 = mysqli_query($this->local_connection,$sql2);
					$row_count2 = mysqli_num_rows($result2);
					$sstockist_array = mysqli_fetch_assoc($result2);	
					
					echo "<br>".$sql3="UPDATE tbl_user SET `sstockist_id` = ".$sstockist_array['external_id']."
					where id = ".$row_sp['id'];
					$result3 = mysqli_query($this->local_connection,$sql3);
				}
			}	
		}else
			return $row_count;	
	}
	//delete functionality
	
	public function deleteSuperstockistById($user_id,$user_id1){
		
		
		$tbl_user = "UPDATE  tbl_user SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->local_connection,$tbl_user);
		
		$tbl_users = "UPDATE  tbl_users SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->common_connection,$tbl_users);

        $this->stockisttest($user_id,$user_id1);
		//$tbl_user1 = "UPDATE  tbl_user SET external_id='$user_id1' WHERE external_id='$user_id'";
		//mysqli_query($this->local_connection,$tbl_user1);

		$tbl_shops = "UPDATE  tbl_shops SET sstockist_id='$user_id1' WHERE sstockist_id='$user_id'";
		mysqli_query($this->local_connection,$tbl_shops);
		
		
		$tbl_shops = "UPDATE  tbl_shops SET shop_added_by='$user_id1' WHERE shop_added_by='$user_id'";
		mysqli_query($this->local_connection,$tbl_shops);
		
		//set default stockist or superstockist if default ss,s want to delete
		$tbl_user = "select is_default_user,firstname FROM  tbl_user  WHERE id='$user_id' ";
		$result1 = mysqli_query($this->local_connection,$tbl_user);
		$row_count = mysqli_num_rows($result1);
		
		if($row_count > 0){	
			$row = mysqli_fetch_assoc($result1);
			if($row['is_default_user']!=0){
				$tbl_user = "UPDATE  tbl_user SET is_default_user='1' WHERE id='$user_id1'";
				mysqli_query($this->local_connection,$tbl_user);				
			}
		}
		
	}
	//function that removes a certain element
	function removeFromString($str, $item) {
		$parts = explode(',', $str);

		while(($i = array_search($item, $parts)) !== false) {
			unset($parts[$i]);
		}

		return implode(',', $parts);
	}
	//for external id remove
	public function stockistremove($user_id){
		$sql2="SELECT `id`,`external_id`
					FROM tbl_user   WHERE   external_id LIKE '$user_id' or  external_id LIKE '%,$user_id%' or 
					external_id LIKE '%$user_id,%' ";
		$result2 = mysqli_query($this->local_connection,$sql2);
		
		while($rowcom = mysqli_fetch_array($result2)){
			$stockist_array['id']=$rowcom['id'];
			$stockist_array['external_id']=$rowcom['external_id'];
			$stockist_array1[]=$stockist_array;
		}
		//$stockist_array = mysqli_fetch_assoc($result2);	
		//echo "<pre>";print_r($stockist_array1);
		foreach($stockist_array1 as $key=>$value){
			$stockist_array1[$key]['external_id']=$this->removeFromString($value['external_id'],$user_id);
		}
		//echo "after: <pre>";print_r($stockist_array1);
		foreach($stockist_array1 as $key => $value){
			$tempextid=$value['external_id'];
			$tempid=$value['id'];
			$tbl_user1 = "UPDATE  tbl_user SET external_id='$tempextid' WHERE  id = $tempid ";
			mysqli_query($this->local_connection,$tbl_user1);
		}
	}
	//for extenal id replacement
	public function stockisttest($user_id,$user_id1){
		$sql2="SELECT `id`,`external_id`
					FROM tbl_user   WHERE   external_id LIKE '$user_id' or  external_id LIKE '%,$user_id%' or 
					external_id LIKE '%$user_id,%' ";
		$result2 = mysqli_query($this->local_connection,$sql2);
		
		while($rowcom = mysqli_fetch_array($result2)){
			$stockist_array['id']=$rowcom['id'];
			$stockist_array['external_id']=$rowcom['external_id'];
			$stockist_array1[]=$stockist_array;
		}
		//$stockist_array = mysqli_fetch_assoc($result2);	
		//echo "<pre>";print_r($stockist_array1);
		foreach($stockist_array1 as $key=>$value){
				if (strpos($value['external_id'], $user_id1) !== false) {	
					if (strpos(','.$value['external_id'], $user_id1) !== false) {	
						$stockist_array1[$key]['external_id']=str_replace(','.$user_id,'',$value['external_id']);
					}else 
					if (strpos($value['external_id'].',', $user_id1) !== false) {	
						$stockist_array1[$key]['external_id']=str_replace($user_id.',','',$value['external_id']);
					}else if (strpos(','.$value['external_id'].',', $user_id1) !== false) {
						$stockist_array1[$key]['external_id']=str_replace($user_id.',','',$value['external_id']);
					}else{
						$stockist_array1[$key]['external_id']=str_replace($user_id,'',$value['external_id']);
					}
					
				}else{
					$stockist_array1[$key]['external_id']=str_replace($user_id,$user_id1,$value['external_id']);
				}
		}
		//echo "after: <pre>";print_r($stockist_array1);
		foreach($stockist_array1 as $key => $value){
			$tempextid=$value['external_id'];
			$tempid=$value['id'];
			$tbl_user1 = "UPDATE  tbl_user SET external_id='$tempextid' WHERE  id = $tempid ";
			mysqli_query($this->local_connection,$tbl_user1);
		}
		
	}
	public function deleteStockistById($user_id,$user_id1){
		
		$tbl_user = "UPDATE  tbl_user SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->local_connection,$tbl_user);
		
		$tbl_users = "UPDATE  tbl_users SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->common_connection,$tbl_users);

        $this->stockisttest($user_id,$user_id1);
	//	$tbl_user1 = "UPDATE  tbl_user SET external_id='$user_id1' WHERE external_id='$user_id'";
	//	mysqli_query($this->local_connection,$tbl_user1);
		
		
		//stockist
		$tbl_shops = "UPDATE  tbl_shops SET stockist_id='$user_id1' WHERE stockist_id='$user_id'";
		mysqli_query($this->local_connection,$tbl_shops);
		//stockist
		$tbl_shop_assignedto_users = "UPDATE  tbl_shop_assignedto_users SET user_id='$user_id1' WHERE user_id='$user_id'";
		mysqli_query($this->local_connection,$tbl_shop_assignedto_users);
		
		$tbl_shops = "UPDATE  tbl_shops SET shop_added_by='$user_id1' WHERE shop_added_by='$user_id'";
		mysqli_query($this->local_connection,$tbl_shops);
		
		$tbl_user = "select is_default_user,firstname FROM  tbl_user  WHERE id='$user_id' ";
		$result1 = mysqli_query($this->local_connection,$tbl_user);
		$row_count = mysqli_num_rows($result1);
		//set default stockist or superstockist if default ss,s want to delete
		
		if($row_count > 0){	
			$row = mysqli_fetch_assoc($result1);
			if($row['is_default_user']!=0){
				$tbl_user = "UPDATE  tbl_user SET is_default_user='1' WHERE id='$user_id1'";
				mysqli_query($this->local_connection,$tbl_user);				
			}
		}
		
	}
	public function deleteSalespersonById($user_id){
		$user_data = $this->getLocalUserDetails($user_id);
		$external_id=$user_data['external_id'];
		$tbl_user = "UPDATE  tbl_user SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->local_connection,$tbl_user);
		
		$tbl_user_working_area = "UPDATE  tbl_user_working_area SET suburb_ids='' WHERE user_id='$user_id'";
		mysqli_query($this->local_connection,$tbl_user_working_area);
		
		$tbl_users = "UPDATE  tbl_users SET isdeleted='1' WHERE id='$user_id'";
		mysqli_query($this->common_connection,$tbl_users);
		
		/*$tbl_shops = "UPDATE  tbl_shops SET shop_added_by='$external_id' WHERE shop_added_by='$user_id'";
		mysqli_query($this->local_connection,$tbl_shops);		*/
	}
	
	public function deleteShopbyid($id){
		$tbl_shops = "UPDATE  tbl_shops SET isdeleted='1'   WHERE id='$id'";
		mysqli_query($this->local_connection,$tbl_shops);
		
	}
	
	public function deleteAreabyid($id){		
		$tbl_shops = "UPDATE  tbl_surb SET isdeleted='1'   WHERE id='$id'";
		mysqli_query($this->local_connection,$tbl_shops);		
		
		/*update user's region */
		$tbl_user_working_area4 = "UPDATE  tbl_user_working_area SET suburb_ids= REPLACE(suburb_ids, ',$id,', '') WHERE suburb_ids LIKE '%,$id,'";
		mysqli_query($this->local_connection,$tbl_user_working_area4);		
		$tbl_user_working_area2 = "UPDATE  tbl_user_working_area SET suburb_ids= REPLACE(suburb_ids, ',$id', '') WHERE suburb_ids LIKE '%,$id'";
		mysqli_query($this->local_connection,$tbl_user_working_area2);
		$tbl_user_working_area3 = "UPDATE  tbl_user_working_area SET suburb_ids= REPLACE(suburb_ids, '$id,', '') WHERE suburb_ids LIKE '$id,%'";
		mysqli_query($this->local_connection,$tbl_user_working_area3);
		$tbl_user_working_area1 = "UPDATE  tbl_user_working_area SET suburb_ids= REPLACE(suburb_ids, '$id', '') WHERE suburb_ids LIKE '$id'";
		mysqli_query($this->local_connection,$tbl_user_working_area1);
		$tbl_shops = "UPDATE  tbl_shops SET suburbid = '' WHERE suburbid = '$id'";
		mysqli_query($this->local_connection,$tbl_shops);
	}
	
	public function deleteSubareabyid($id){
		$tbl_shops = "UPDATE  tbl_subarea SET isdeleted='1'   WHERE subarea_id='$id'";
		mysqli_query($this->local_connection,$tbl_shops);
	}
	public function getLocalUserOtherDetails($user_id,$user_type='') {	
		$sql1="SELECT `userid`, `accname`,  `accno`, `accbrnm`, `accifsc`, `phone_no`, `tollfree_no`, `bank_name`,`website`, `declaration`
		FROM tbl_user_details where userid = '".$user_id."' AND isdeleted!='1' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);	
		}else
			return $row_count;		
	}
	public function addLocalUserOtherDetails($user_id) {
		extract ($_POST);
		$fields = '';
		$values = ''; 
		
		if($phone_no != '')
		{
			$fields.= ",`phone_no`";
			$values.= ",'".$phone_no."'";
		}
		if($tollfree_no != '')
		{
			$fields.= ",`tollfree_no`";
			$values.= ",'".$tollfree_no."'";
		}
		if($website != '')
		{
			$fields.= ",`website`";
			$values.= ",'".$website."'";
		}
		if($declaration != '')
		{
			$fields.= ",`declaration`";
			$values.= ",'".$declaration."'";
		}
		if($accname != '')
		{
			$fields.= ",`accname`";
			$values.= ",'".$accname."'";
		}
		if($accno != '')
		{
			$fields.= ",`accno`";
			$values.= ",'".$accno."'";
		}
		if($accbrnm != '')
		{
			$fields.= ",`accbrnm`";
			$values.= ",'".$accbrnm."'";
		}
		if($accifsc != '')
		{
			$fields.= ",`accifsc`";
			$values.= ",'".$accifsc."'";
}		
		if($bank_name != '')
		{
			$fields.= ",`bank_name`";
			$values.= ",'".$bank_name."'";
		}
		
		$user_other_details = "INSERT INTO tbl_user_details (`userid` $fields) 
		VALUES('".$user_id."' $values)";
		mysqli_query($this->local_connection,$user_other_details);		
		$this->commonObj->log_add_record('tbl_user_details',$user_id,$user_other_details);	
	}
	public function updateLocalUserOtherDetails($user_id) {
		extract ($_POST);		
		$values = ''; 
		
		if($phone_no != '')
		{
			$values.= ", `phone_no` = '".$phone_no."'";
		}
		if($tollfree_no != '')
		{
			$values.= ", `tollfree_no` = '".$tollfree_no."'";
		}
		if($website != '')
		{
			$values.= ", `website` = '".$website."'";
		}
		if($declaration != '')
		{
			$values.= ", `declaration` = '".$declaration."'";
		}
		if($accname != '')
		{
			$values.= ", `accname` = '".$accname."'";
		}
		if($accno != '')
		{
			$values.= ", `accno` = '".$accno."'";
		}
		if($accbrnm != '')
		{
			$values.= ", `accbrnm` = '".$accbrnm."'";
		}
		if($accifsc != '')
		{
			$values.= ", `accifsc` = '".$accifsc."'";
		}
		if($bank_name != '')
		{
			$values.= ", `bank_name` = '".$bank_name."'";
		}	
		$user_other_details = "UPDATE tbl_user_details SET userid='$user_id' $values WHERE userid='$user_id'";				
			
		mysqli_query($this->local_connection,$user_other_details);
		$this->commonObj->log_update_record('tbl_user_details',$user_id, $user_other_details);
	}
	public function assignRegion() {	
		extract ($_POST);	
		$region_id = $assign;			
		if(count($assign) > 1){
			$region_id = implode(',',$assign);
		}else{
			$region_id = $assign[0];
		}				
		
		$values.= " `suburb_ids`= '".$region_id."'";
		$update_user_sql = "UPDATE tbl_user_working_area SET $values WHERE user_id='$sales_p_id'";
		return mysqli_query($this->local_connection,$update_user_sql);
		$this->commonObj->log_update_record('tbl_user_working_area',$sales_p_id,$update_user_sql);
	}
	public function getAssignedRegion() {	
		$sql = "SELECT GROUP_CONCAT(suburb_ids) AS assigned_regions
		FROM tbl_user_working_area 
		WHERE suburb_ids != '' AND 
		user_id IN (SELECT id FROM tbl_user WHERE user_type = 'salesperson' AND isdeleted != 1)";	
		$result1 = mysqli_query($this->local_connection,$sql);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);	
		}else
			return $row_count;	
	}
	public function getAllLocalUser($user_type){		
		$sql1="SELECT `id`, `surname`, `firstname`, `username`, 
		`pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, 
		`reset_key`, `suburbid`, `is_default_user`, `user_status`
		FROM tbl_user where user_type = '".$user_type."' order by is_default_user desc, firstname asc";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}
}
?>